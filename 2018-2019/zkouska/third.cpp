#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <map>
#include <set>
#include <list>
#include <vector>
#include <stack>
#include <queue>
#include <deque>
#include <algorithm>
#include <memory>
using namespace std;

class CTransport{
public:
    // def constructor
    CTransport(){};
    // destructor
    ~CTransport(){};
    void printStations()
    {
        for(auto & it: stations)
        {
            cout << it.first << ": ";
            for(auto & adjacent: it.second)
            {
                cout << adjacent << ", ";
            }
            cout << endl;
        }
    }
    void AddLine (istream &is)
    {
        string lastStation, currentStation;
        bool firstStation = true; // Indicates the first station in stream
        while(getline(is, currentStation))
        {
            vector<string> adjacent;
            // Station doesn't exist
            if(stations.find(currentStation) == stations.end())
            {
                if(!firstStation)
                {
                    // Both-way adjacency
                    adjacent.push_back(lastStation);
                    stations[lastStation].push_back(currentStation);
                }
                stations.insert(make_pair(currentStation, adjacent));
            }
            else // Station does exist
            {
                if(!firstStation)
                {
                    // Both-way adjacency
                    // Checking if station is not in adjacent already
                    if(find(stations[currentStation].begin(), stations[currentStation].end(), lastStation) == stations[currentStation].end())
                        stations[currentStation].push_back(lastStation);
                    if(find(stations[lastStation].begin(), stations[lastStation].end(), currentStation) == stations[lastStation].end())
                        stations[lastStation].push_back(currentStation);
                }
            }
            // Preserve currentStation for next loop (adjacency)
            lastStation = currentStation;
            firstStation = false;
        }
    }
    int Count () const
    {
        queue<string> queue;
        set<string> visited;
        int islandCount = 0;

        for(auto & station: stations)
        {
            if(visited.find(station.first) != visited.end())
                continue;

            queue.push(station.first);
            while(!queue.empty())
            {
                string currentStation = queue.front();
                queue.pop();
                visited.insert(currentStation);

                // Iterating over adjacent stations
                vector<string> adjacents = stations.find(currentStation)->second;
                for(auto & adjacent: adjacents)
                {
                    if(visited.find(adjacent) == visited.end())
                    {
                        queue.push(adjacent);
                        visited.insert(adjacent);
                    }
                }
            }
            islandCount++;
        }
        // DEBUG print of returned values
        // for(auto & it: visited)
        // {
        //     cout << it << ", ";
        // }
        // cout << endl;
        return islandCount;
    }

private:
    // Station name <-> Adjacent stations
    map<string, vector<string>> stations;
};

int main()
{
    CTransport t0;
    istringstream iss;
    iss.clear();
    iss.str("Newton\n" "Black Hill\n" "Wood Side\n" "Green Hill\n" "Lakeside\n");
    t0.AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 1" << endl;
    iss.clear();
    iss . str("Little Newton\nLittle Burnside\nCastle Hill\nNewton Crossroad\nLakeside Central\n");
    t0 . AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 2" << endl;
    iss . clear();
    iss . str("Waterton West\nWaterton Central\nWaterton East\nWaterton Woods\nLittle Waterton\nWaterton West\n");
    t0 . AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 3" << endl;
    iss . clear();
    iss . str("Little Waterton\nLakeside Central\n");
    t0 . AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 2" << endl;
    iss . clear();
    iss . str("Great Newton\nLittle Burnside\nGreen Hill\nWood Side\n");
    t0 . AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 1" << endl;
    iss . clear();
    iss . str("Sodor Ironworks\nSodor Steamworks\nKnapford\nMaron\n");
    t0 . AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 2" << endl;
    iss . clear();
    iss . str("Tidmouth\nGordons Hill\nSuderry\nKnapford\nGreat Waterton\nBrendam Docks\n");
    t0 . AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 2" << endl;
    iss . clear();
    iss . str("Newton\nNewton Crossroad\nTidmouth\nBrendam Docks\n");
    t0 . AddLine(iss);
    cout << "res : " << t0.Count() << "  ref : 1" << endl;

    return 0;
}
