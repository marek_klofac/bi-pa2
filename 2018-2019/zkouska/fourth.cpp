#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <map>
#include <set>
#include <list>
#include <vector>
#include <stack>
#include <queue>
#include <deque>
#include <algorithm>
#include <memory>
using namespace std;

class CTransport{
public:
    // def constructor
    CTransport(){};
    // destructor
    ~CTransport(){};
    void printStations()
    {
        for(auto & it: stations)
        {
            cout << it.first << ": ";
            for(auto & adjacent: it.second)
            {
                cout << adjacent << ", ";
            }
            cout << endl;
        }
    }
    void AddLine (istream &is)
    {
        string lastStation, currentStation;
        bool firstStation = true;
        while(getline(is, currentStation))
        {
            vector<string> adjacents;
            if(stations.find(currentStation) == stations.end())
            {
                if(!firstStation)
                {
                    adjacents.push_back(lastStation);
                    if(find(stations[lastStation].begin(), stations[lastStation].end(), currentStation) == stations[lastStation].end())
                        stations[lastStation].push_back(currentStation);
                }
                stations.insert(make_pair(currentStation, adjacents));
            }
            else
            {
                if(!firstStation)
                {
                    if(find(stations[currentStation].begin(), stations[currentStation].end(), lastStation) == stations[currentStation].end())
                        stations[currentStation].push_back(lastStation);
                    if(find(stations[lastStation].begin(), stations[lastStation].end(), currentStation) == stations[lastStation].end())
                        stations[lastStation].push_back(currentStation);
                }
            }
            lastStation = currentStation;
            firstStation = false;
        }
    }
    set<string> MeetingPoints (const vector<string> &fromList, int &cost)
    {
        // Cost sum to all stations fromList <-> Name of the station
        map<int, vector<string>> allCosts;
        int connectedStationsToAllDest = 0;
        // For each node in graph
        for(auto & station: stations)
        {
            int costSum = 0;
            int connectivityToDest = 0;
            // Search shortest path to each station fromList
            for(auto & destination: fromList)
            {
                if(stations.find(destination) == stations.end())
                {
                    cost = 0;
                    return set<string>();
                }

                set<string> visited;
                queue<pair<string, int>> queue;
                queue.push(make_pair(station.first, 0));
                while(!queue.empty())
                {
                    string currentStation = queue.front().first;
                    int currentCost = queue.front().second;
                    queue.pop();
                    if(currentStation == destination)
                    {
                        costSum += currentCost;
                        connectivityToDest++;
                        break;
                    }
                    for(auto & adjacent: stations[currentStation])
                    {
                        if(visited.find(adjacent) == visited.end())
                        {
                            visited.insert(adjacent);
                            queue.push(make_pair(adjacent, currentCost + 1));
                        }
                    }
                }
            }
            if(connectivityToDest == int(fromList.size()))
            {
                connectedStationsToAllDest++;
                allCosts[costSum].push_back(station.first);
            }
        }

        if(connectedStationsToAllDest == 0)
            return set<string>();

        set<string> result;
        for(auto & station: allCosts.begin()->second)
        {
            result.insert(station);
        }
        cost = allCosts.begin()->first;
        return result;
    }

private:
    // Station name <-> Adjacent stations
    map<string, vector<string>> stations;
};

int main()
{
    CTransport t0;
    istringstream iss;

    iss.clear();
    iss.str(
        "1\n"
        "2\n"
        "3\n"
        "4\n"
        "5\n"
        "6\n"
        "7\n"
        "8\n");
    t0.AddLine(iss);
    iss.clear();
    iss.str(
        "12\n"
        "11\n"
        "4\n"
        "9\n"
        "10\n");
    t0.AddLine(iss);
    iss.clear();
    iss.str(
        "15\n"
        "11\n"
        "13\n"
        "14\n");
    t0.AddLine(iss);
    iss.clear();
    iss.str(
        "7\n"
        "16\n"
        "17\n");
    t0.AddLine(iss);
    iss.clear();
    iss.str(
        "20\n"
        "19\n"
        "17\n"
        "18\n");
    t0.AddLine(iss);
    iss.clear();
    iss.str(
        "21\n"
        "22\n"
        "23\n"
        "25\n"
        "24\n"
        "21\n");
    t0.AddLine(iss);
    iss.clear();
    iss.str(
        "26\n"
        "27\n"
        "28\n");
    t0.AddLine(iss);
    iss.clear();
    iss.str(
        "27\n"
        "29\n"
        "30\n");
    t0.AddLine(iss);

    int cost = 5381;
    assert(t0.MeetingPoints({"1", "10", "17"}, cost) == set<string>({"4"}) && cost == 10);
    assert(t0.MeetingPoints({"2", "5", "11"}, cost) == set<string>({"4"}) && cost == 4);
    assert(t0.MeetingPoints({"12", "10"}, cost) == set<string>({"10", "9", "4", "11", "12"}) && cost == 4);
    assert(t0.MeetingPoints({"2", "9", "19"}, cost) == set<string>({"4"}) && cost == 9);
    assert(t0.MeetingPoints({"15", "12", "13"}, cost) == set<string>({"11"}) && cost == 3);
    cost = 5381;
    assert(t0.MeetingPoints({"21", "26"}, cost) == set<string>() && cost == 5381);
    cost = 1256;
    assert(t0.MeetingPoints({"10", "28"}, cost) == set<string>() && cost == 1256);
    assert(t0.MeetingPoints({"21", "25"}, cost) == set<string>({"21", "24", "25"}) && cost == 2);
    assert(t0.MeetingPoints({"21", "21"}, cost) == set<string>({"21"}) && cost == 0);
    assert(t0.MeetingPoints({"23", "21"}, cost) == set<string>({"21", "22", "23"}) && cost == 2);
    assert(t0.MeetingPoints({"12", "20"}, cost) == set<string>({"12", "11", "4", "5", "6", "7", "16", "17", "19", "20"}) && cost == 9);
    assert(t0.MeetingPoints({"50"}, cost) == set<string>() && cost == 0);
    assert(t0.MeetingPoints({"13", "10"}, cost) == set<string>({"10", "11", "13", "4", "9"}) && cost == 4);
    assert(t0.MeetingPoints({"17", "19"}, cost) == set<string>({"17", "19"}) && cost == 1);
    cost = 1999;
    assert(t0.MeetingPoints({"29", "25"}, cost) == set<string>() && cost == 1999);
    assert(t0.MeetingPoints({"8", "5"}, cost) == set<string>({"5", "6", "7", "8"}) && cost == 3);
    assert(t0.MeetingPoints({"4", "4"}, cost) == set<string>({"4"}) && cost == 0);

    return 0;
}
