#include <iostream>

int main()
{
	int cols = 0;
	std::cin >> cols;
	if(cols < 1)
	{
		std::cout << "You have to enter a positive number." << std::endl;
		return(1);
	}
	int rows = cols/2+1;
	int ** array = new int *[rows];
	for(int i=0; i<rows; i++)
	{
		if(cols != 0)
		{
			array[i] = new int[cols];
			for(int j=0; j<cols; j++)
			{
				array[i][j] = j;
				std::cout << array[i][j] << " ";
			}
			std::cout << std::endl;
			cols = cols/2;
		}
	}
	//Clenaup
	for(int i = 0; i < rows; ++i) {
		delete [] array[i];
	}
	delete [] array;
	return(0);
}

