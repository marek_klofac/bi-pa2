#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

using namespace std;

string * resizeArr(string * oldArr, int & arrLen)
{
	size_t newLen = arrLen * 2;
    string * newArr = new string[newLen];
    for (int i = 0; i < arrLen; ++i)
    {
    	newArr[i] = oldArr[i];
    }
    arrLen = newLen;
    delete [] oldArr;
    return newArr;
}

int main()
{
	int arrLen = 10;
	string * array = new string [arrLen];
	string word;
	int wordCount = 0;
	// Reading from stdin until EOF
	while(!cin.eof())
	{
		cin >> word;
		if(cin.peek() == EOF)
			break;
		if(wordCount >= arrLen)
			array = resizeArr(array, arrLen);
		array[wordCount] = word;
		wordCount++;
	}
	// Print array
	ofstream file;
  	file.open ("output.txt");
	for (int j = 0; j < wordCount; ++j)
	{
		cout << array[j] << endl;
  		file << array[j] << endl;
	}
	delete [] array;
	return 0;
}