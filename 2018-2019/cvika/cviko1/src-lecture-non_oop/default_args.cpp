/* default_args.cpp */

// function default parameters in C++

#include <cstdlib>
#include <iostream>

using namespace std;

void print ( int x, int y = 0, int z = 0 )
{
  cout << "x = " << x << " y = " << y << " z = " << z << endl;
}
  
int main ( )
{
  print ( 10, 20, 30 );
  print ( 10, 20 );
  print ( 10 );
  return 0;
}
