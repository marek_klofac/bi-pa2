/* reference.cpp */

// i/o parameters in C and reference type in C++

#include <cstdlib>
#include <iostream>

using namespace std;

void swapC ( int *px, int *py )
{
  int tmp = *px;
  *px = *py;
  *py = tmp;
}

void swapCPP ( int& x, int &y )
{
  int tmp = x;
  x = y;
  y = tmp;
}

int main ( )
{
  int a = 10, b = 20; 
  cout << "a = " << a << " b = " << b << endl;
  swapC(&a, &b);
  cout << "past swap: a = " << a << " b = " << b << endl;
  swapCPP(a, b);
  cout << "past swap: a = " << a << " b = " << b << endl;
  
/* errors:
  float x, y; const int c = 100;
  swapC(&x, &y);
  swapC(&a, &c);
  swapCPP(x, y);
  swapCPP(a, c);
*/
  return 0;
}
