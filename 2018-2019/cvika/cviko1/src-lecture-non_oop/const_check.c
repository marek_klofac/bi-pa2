/* const_check.c */

/* Compile with C comipler, no C++! Shows insufficient checks of const qualifier in C language.
 * Only warning reported when compiling by gcc:
 *   warning: passing argument 2 of ‘swapC’ discards ‘const’ qualifier from pointer target type.
 * Constant in C will be overwriten in run-time
 * Does not compile in C++ compiler:
 *   error: invalid conversion from ‘const int*’ to ‘int*’.
 */

#include <stdio.h>
#include <stdlib.h>

void swapC( int *px, int *py )
{
  int tmp = *px;
  *px = *py;
  *py = tmp;
}

int main ( )
{
  int a = 10, b = 20; 
  const int c = 100;
  printf ( "original:     a = %d b = %d\n", a, b );
  swapC ( &a, &b );
  printf ( "swapped:      a = %d b = %d\n", a, b );
  swapC ( &a, &c);
  printf ( "another swap: a = %d c = %d\n", a, c );
  return 0;
}

