/* inline_fun.cpp */

// inline functions in C++

#include <cstdlib>
#include <iostream>

using namespace std;

inline int max ( int x, int y )
{ 
  return x>=y ? x : y; 
}

#define MAX(x,y) ((x)>=(y) ? (x) : (y))

int main ( )
{ 
  int a=10;
  
  cout << "max " << max ( a++, 4 ); 
  cout << ' ' << a << endl;
  cout << "max " << max ( a++, 4 ); 
  cout << ' ' << a << endl;
  
  
  cout << "MAX " << MAX ( a++, 4 ); 
  cout << ' ' << a << endl; // !!!
  cout << "MAX " << MAX ( a++, 4 ); 
  cout << ' ' << a << endl; // !!!
  return 0;
}
