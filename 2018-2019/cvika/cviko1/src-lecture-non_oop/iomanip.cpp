/* iomanip.cpp */

// output manipulators in C++

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

int main ( )
{
  int x = -10;
  cout << "decimal " << x << endl;
  cout << "15 characters " << setw(15) << setfill('.') << x << endl;
  cout << "octal " << oct << showbase << x << endl;
  cout << "hexadecimal " << hex << x << endl;
  cout << "decimal " << dec << x << endl;
  return 0;
}
