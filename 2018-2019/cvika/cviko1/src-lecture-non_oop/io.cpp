/* io.cpp */

// i/o in C and in C++ 

#include <cstdlib>
#include <cstdio>
#include <iostream>

using namespace std;

int main ( )
{
  int x;
// change x to float;

// i/o C style
  printf ( "Enter an integer:\n" );
  scanf ( "%d", &x );
  printf ( "you have entered - C style: %d\n", x );
  
// i/o C++ style
  cout << "Enter an integer:" << endl;
  cin >> x;
  cout << "you have entered - C++ style: " << x << endl;
  
  return 0;
}
