/* fun_overload.cpp */

// function overload in C++

#include <cstdlib>
#include <iostream>

using namespace std;

int cube ( int x )
{
  return x * x * x;
}

float cube ( float x )
{
  return x * x * x;
}

int main ( )
{
  int a = 5;
  float b = 4.2;
  cout << a << " ^ 3 = " << cube ( a ) << endl;
  cout << b << " ^ 3 = " << cube ( b ) << endl;
  return 0;
}
