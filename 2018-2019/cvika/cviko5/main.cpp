//https://pastebin.com/1Kxz745y

#include "World.h"

int main()
{
   //-----------------------------------------------------
   // Task #1
   /*~~~~Well defined world */
   World dark( "Dark", 5 );
   //std::cout << dark << std::endl;
 
   /*~~~Operators correctly overloaded*/
   //World light( "Light", 11 );
   //for( int i = 0; i < 10; ++i )
       //light += {i, i, i * 2, i * 5};
 
   //std::cout << light << std::endl;
 
   /*~~~~Copy ctor*/
   //World lightCopy( light );
   /*~~~~Assignment operator*/
   //lightCopy = lightCopy += {30, 30, 2, 1};
   //std::cout << lightCopy << std::endl;
 
   //lightCopy = dark;
   //std::cout << lightCopy << std::endl;
 
   //-----------------------------------------------------
   // Task #2
   /*~~~operator-=*/
   //World a("Gondor", 40);
   //std::cout << a << std::endl;
 
   //a += {0,0,5,5};
   //std::cout << a << std::endl;
   //a -= {0,0,5,5};
   //std::cout << a << std::endl;
   //a += {1,1,5,5};
   //std::cout << a << std::endl;
 
   //-----------------------------------------------------
   // Task #3 & #4
   // Create your own test cases to demonstrate required functionality.
 
   return 0;
}
