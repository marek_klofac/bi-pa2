#pragma once

#include "Block.h"
#include <string>
#include <vector>
#include <iostream>

class World
{
private:
    std::string name;
    int numberOfObjects = 1000;
    std::vector <Block> blocks;

public:
    World();
    World(const std::string name, int numberOfObjects);
    World (const World & other);
    ~World();

};

ostream & operator << (ostream & os, const World & world);