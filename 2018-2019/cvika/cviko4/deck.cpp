#include <string>
#include <iostream>
#include <algorithm>
#include "deck.hpp"

using namespace std;
const int MAXIMUM_DECK_SIZE = 20;

Deck::Deck(string name) : name(name) {
    cards_size = MAXIMUM_DECK_SIZE;
    cards_count = 0;
    cards = new Card[cards_size];
}

Deck::~Deck() {
    delete[] cards;
}

Deck::Deck(const Deck &other_deck) {
    name = other_deck.name;
    cards_count = other_deck.cards_count;
    cards_size = other_deck.cards_size;
    cards = new Card[cards_size];
    for (int i = 0; i < cards_count; ++i) {
        cards[i] = other_deck.cards[i];
    }
}

void Deck::Print() const {
    cout << "Deck " << name << " has " << cards_count << " cards." << endl;
    for (int i = 0; i < cards_count; ++i) {
        cards[i].Print();
    }
}

Deck & Deck::operator += (const Card & card)
{
    if(cards_count > cards_size)
        throw("Card could not be added. Too much cards in deck.");
    this->cards[cards_count] = card;
    this->cards_count++;

    return(*this);
}

ostream & operator << (ostream & os, const Deck & deck)
{
    os << "Deck name: " << deck.name << endl;
    os << "Card count: " << deck.cards_count << endl;
    for(int i = 0; i < deck.cards_count; i++)
    {
        os << "--------------" << endl;
        os << "CARD " << i << endl;
        os << deck.cards[i];
    }
    return(os);
}

Card & Deck::operator [] (int i)
{
    cout << "Cards count: " << cards_count << endl;
    if(i < 0 || i >= cards_count)
        throw std::out_of_range("Index: " + to_string(i) + " | Cards count: " + to_string(cards_count));
    return(cards[i]);
}

Deck & Deck::operator + (const Deck & otherDeck)
{
    this->name += otherDeck.name;
    this->cards_count += otherDeck.cards_count;
    if(this->cards_count > this->cards_size)
        throw("Number of cards exceeded limit after merging two decks.");
    int starting_point = this->cards_count - otherDeck.cards_count;
    for(int i = 0; i < otherDeck.cards_count; i++)
    {
        this->cards[starting_point] = otherDeck.cards[i];
        starting_point++;
    }
    return(*this);
}

/*
Deck & Deck::operator + (const Deck & a, const Deck & b)
{
    Deck mergedDeck = Deck(a.name + b.name);
    mergedDeck.cards_count = a.cards_count + b.cards_count;
    if(mergedDeck.cards_count > mergedDeck.cards_size)
        throw("Number of cards exceeded limit after merging two decks.");
    int starting_poiint = 0;
    while(i < a.cards_count)
    {
        mergedDeck.cards[i] = a.cards[i];
        i++;
    }
    int j = 0;
    while(i < b.cards_count)
    {
        mergedDeck.cards[i] = b.cards[j];
        j++;
    }
    return(*this);
}*/