#include "card.hpp"
#include <iostream>

void Card::Print()const {
	cout << "Card name: " << name << " mana: " << mana << " attack: " << attack << endl;
}

ostream & operator << (ostream & os, const Card & card)
{
    os << "Card name: " << card.name << endl;
	os << "Mana: " << card.mana << endl;
	os << "Attack: " << card.attack << endl;
    return(os);
}