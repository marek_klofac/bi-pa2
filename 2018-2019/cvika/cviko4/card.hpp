#pragma once

#include <string>

using namespace std;

class Card {
public:
	Card(const string &name = "unknown", int mana = 0, int attack = 0) : name(name), mana(mana), attack(attack) {}
	void Print() const;

private:
	string name;
	int mana;
	int attack;
	friend ostream & operator << (ostream & os, const Card & card);
};