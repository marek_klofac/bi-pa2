#pragma once
#include "card.hpp"
#include <iostream>
using namespace std;
class Deck {
public:
	Deck(string name);
	~Deck();
	Deck(const Deck& other_deck); //Copy constructor, don't remove unless you know what you are doing
	void Print() const;
	Deck & operator += (const Card & card);
	Card & operator [] (int i);
	Deck & operator + (const Deck & otherDeck);
	//Deck & Deck::operator + (const Deck & a, const Deck & b);
private:
	string name;
	Card * cards;
	int cards_count;
	int cards_size;
	friend ostream & operator << (ostream & os, const Deck & deck);
};

ostream & operator << (ostream & os, const Deck & deck);