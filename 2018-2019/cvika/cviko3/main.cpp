#include "GameArray.h"
//https://pastebin.com/XRNVSq7v

int main()
{
	GameArray game = GameArray(3,3);
	game.print();
	std::cout << "--------------------" << std::endl;
	game.addBoat(0,0,1,2); //(row_start, col_start, row_length, col_length)
	game.print();
	std::cout << "--------------------" << std::endl;
	game.addBoat(1,1,2,1);
	game.print();
	std::cout << "--------------------" << std::endl;
}

