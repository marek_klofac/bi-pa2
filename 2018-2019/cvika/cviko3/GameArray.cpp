#include "GameArray.h"

GameArray::GameArray(int rows, int cols)
{
	this->rows = rows;
	this->cols = cols;
	array = new State * [rows];
	for(int i = 0; i < rows; ++i)
    	array[i] = new State [cols];
	//Init of array to be all WATER
	for(int i = 0; i < rows; ++i)
	{
		for(int j = 0; j < cols; ++j)
		{
			array[i][j] = WATER;
		}
	}
}

GameArray::~GameArray()
{
	for(int i = 0; i < rows; ++i) {
    	delete [] array[i];
	}
	delete [] array;
}

void GameArray::print() const
{
	for(int i = 0; i < rows; ++i)
	{
		for(int j = 0; j < cols; ++j)
		{
			switch(array[i][j])
			{
				case WATER:
					std::cout << "|WATER"; 					
					break;
				case BOAT:
					std::cout << "|BOAT "; 					
					break;
				case SINK_BOAT:
					std::cout << "|SINK "; 					
					break;
				default:
					std::cout << "|empty";
					break;
			}
		}
		std::cout << "|" << std::endl;
	}
}

bool GameArray::checkEmptySpace(int rows, int cols) const
{
	return(array[rows][cols] == WATER);
}

void GameArray::addBoat(int row_start, int col_start, int row_size, int col_size)
{
	if(row_start < 0 || col_start < 0 || row_start > rows || col_start > cols)
		throw InvalidBoatPlacement();

	// TODO - Neni mozne pridat lod pokud jiz souper vystrelil...

	// Case where boat is 1*m
	if(row_size == 1)
	{
		int i = col_start;
		// Checking if there is empty space for the whole boat
		while(i < cols)
		{
			if(!checkEmptySpace(row_start, i)) //Returns true if space is empty
				throw BoatAlreadyExistsOnThisSpace();
			i++;
		}
		if(i < col_size)
			throw NotEnoughSpaceForBoat();
		//Safe to create boat now
		for(int j = col_start; j < col_size; ++j)
			array[row_start][j] = BOAT;
	}
	// Case where boat is m*1
	else if(col_size == 1)
	{
		int i = row_start;
		// Checking if there is empty space for the whole boat
		while(i < rows)
		{
			if(!checkEmptySpace(i, col_start)) //Returns true if space is empty
				throw BoatAlreadyExistsOnThisSpace();
			i++;
		}
		if(i < row_size)
			throw NotEnoughSpaceForBoat();
		//Safe to create boat now
		for(int i = row_start; i < row_size; ++i)
			array[i][col_start] = BOAT;
	}

	//Dont create any boat (create only 1*n or n*1)
}
