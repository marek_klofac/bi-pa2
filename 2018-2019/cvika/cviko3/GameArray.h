#include <iostream>
class InvalidBoatPlacement{};
class BoatAlreadyExistsOnThisSpace{};
class NotEnoughSpaceForBoat{};

class GameArray
{
	private:
		enum State {WATER, BOAT, SINK_BOAT};
		State ** array = nullptr;
		int rows = 0;
		int cols = 0;
	public:
		GameArray(int rows, int cols);
		~GameArray();
		void print() const;
		bool checkEmptySpace(int rows, int cols) const;
		void addBoat(int row_start, int col_start, int row_size, int col_size);
};
