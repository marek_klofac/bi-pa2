#include <string>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <algorithm> 

using namespace std;

enum class WEAPON {axe, sword, bow, spear, crossbow};
//Used for random names for clan members
string CHARS = "abcdefghijklmnopqrstuvwxyz";

class Member
{
	private:
		string name;
		int strength;
		WEAPON weapon;
	public:
		Member(string name, int strength, WEAPON weapon);
		~Member(){};
		void print() const;
		int getStrength() const;
		bool operator < (const Member & member) const;
};

Member::Member(string name, int strength, WEAPON weapon)
{
	this->name = name;
	this->strength = strength;
	this->weapon = weapon;
}

void Member::print() const
{
	cout << "Name: " << name << endl;
	cout << "Strength: " << strength << endl;
}

int Member::getStrength() const
{
	return strength;
}

bool Member::operator < (const Member & member) const
{
	if(this->strength == member.strength)
		return (this->name < member.name);
    return (this->strength < member.strength);
}

class Clan
{
	private:
		string name;
		vector<Member> members;
	public:
		Clan(string name);
		~Clan(){};
		int strengthSum() const;
		void print() const;
		void addMember(const Member & member);
		void addRandomMember();
		string getName() const;
		vector <Member> getMembers() const;
		void setMembers(vector <Member> mems);
		void sortMembers();
};

Clan::Clan(string name)
{
	this->name = name;
}

int Clan::strengthSum() const
{
	int sum = 0;
	for(unsigned int i = 0; i < members.size(); i++)
	{
		sum += members[i].getStrength();
	}
	return sum;
}

void Clan::print() const
{
	cout << "Clan name: " << name << endl;
	cout << "Clan stregth: " << strengthSum() << endl;
	for(unsigned int i = 0; i < members.size(); i++)
	{
		cout << "--------------------" << endl;
		cout << "Member #" << i << endl;
		members[i].print();
	}
}

void Clan::addMember(const Member & member)
{
	members.push_back(member);
}

void Clan::addRandomMember()
{
	string name;
	for(int i = rand()%10+1; i > 0; i--)
	{
		name += CHARS[rand() % 24];
	}
	int strength = rand() % 100 + 1;
	WEAPON weapon = WEAPON(rand() % 5);
	members.push_back(Member(name, strength, weapon));
}

string Clan::getName() const
{
	return name;
}

vector <Member> Clan::getMembers() const
{
	return members;
}

void Clan::setMembers(vector <Member> mems)
{
	members = mems;
}

void Clan::sortMembers()
{
	sort(members.begin(), members.end());
}

Clan * mergeClans(const Clan & a, const Clan & b)
{
	string mergedName;
	vector <Member> a_members = a.getMembers();
	vector <Member> b_members = b.getMembers();
	if (a.strengthSum() > b.strengthSum())
	{
		mergedName.append(a.getName());
		mergedName.append(b.getName());
	}
	else
	{
		mergedName.append(b.getName());
		mergedName.append(a.getName());
	}
	Clan * merged = new Clan(mergedName);
	a_members.insert(a_members.end(), b_members.begin(), b_members.end());
	merged->setMembers(a_members);
	return merged;
}
int main ()
{
	srand (time(NULL));
	Clan clan1 = Clan("Vikingove");
	//Clan clan2 = Clan("Yoldove");
	//clan1.addMember(Member("Mara", 5, WEAPON::axe));
	//clan1.addMember(Member("Honza", 1, WEAPON::axe));
	clan1.addRandomMember();
	clan1.addRandomMember();
	clan1.addRandomMember();
	//clan2.addRandomMember();
	clan1.print();
	cout << "#######################" << endl;
	clan1.sortMembers();
	clan1.print();
	//clan2.print();
	//Clan * mergedClan = mergeClans(clan1, clan2);
	//mergedClan->print();
	//delete mergedClan;
}
