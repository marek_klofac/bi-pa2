#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <map>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
#include <bitset>
using namespace std;
#endif /* __PROGTEST__ */

class TreeNode
{
private:
    TreeNode * leftChild;
    TreeNode * rightChild;
    char symbol;

public:
    TreeNode(): leftChild(NULL), rightChild(NULL){}
    ~TreeNode()
    {
        // Delete both children
        delete leftChild;
        delete rightChild;
    }
    void CreateLeftChild()
    {
        leftChild = new TreeNode();
    }
    void CreateRightChild()
    {
        rightChild = new TreeNode();
    }
    TreeNode * GetLeftChild()
    {
        return leftChild;
    }
    TreeNode * GetRightChild()
    {
        return rightChild;
    }
    char GetSymbol()
    {
        return symbol;
    }
    void SetSymbol(char sym)
    {
        symbol = sym;
    }
    // Pre-order method of printing the tree. For debug only
    void PrintTree(int depth)
    {
        // Leaf, print symbol
        if(leftChild == NULL && rightChild == NULL)
        {
            cout << string(depth, ' ') << "- " << symbol << endl;
            return;
        }
        else
        {
            cout << string(depth, ' ') << "+" << endl;
            // Call recursively on leftChild and rightChild
            leftChild->PrintTree(depth+1);
            rightChild->PrintTree(depth+1);
        }
    }
};

class FileHandler
{
private:
    const char * inFileName;
    const char * outFileName;
    ifstream inFile;
    ofstream outFile;
    char currentByte;
    int currentBit = -1;

    // This function returns the number of encoded characters in last chunk
    int ReadLastChunkSize()
    {
       // Read next 12 bits of last chunk length
        int bits [12] = {};
        for (int i = 0; i < 12; i++)
        {
            bits[i] = ReadBit();
        }
        // Convert int array to string
        std::string tmp;
        for (int i: bits)
        {
            tmp += to_string(i);
        }
        // Converting binary string to int to char
        return stoi(tmp, NULL, 2);
    }
    // This function returns appropriate character for Huffman enconding
    // Depending on bit stream, 0/1 chooses left/right until leaf is hit
    // Saving the depth of recursion in bitsRead for further need
    char DecompressCharacter(TreeNode * root)
    {
        // Leaf encoutered. Return from recursion with symbol
        if(root->GetLeftChild() == NULL && root->GetRightChild() == NULL)
        {
            return root->GetSymbol();
        }
        // Descend to left or right child dependent on bit (0/1) 
        else
        {
            if(ReadBit())
            {
                return DecompressCharacter(root->GetRightChild());
            }
            else
            {
                return DecompressCharacter(root->GetLeftChild());
            }
        }
    }
    // This function reads one character (8 bits) from input stream
    // Source of idea: https://stackoverflow.com/questions/11068204/is-it-possible-to-convert-bitset8-to-char-in-c
    char ReadChar()
    {
        // Read next 8 bits of ASCII character and store them
        int bits [8] = {};
        for (int i = 0; i < 8; i++)
        {
            bits[i] = ReadBit();
        }
        // Convert int array to string
        std::string tmp;
        for (int i: bits)
        {
            tmp += to_string(i);
        }
        // Converting binary string to int to char
        return static_cast<char>(stoi(tmp, NULL, 2));
    }

public:
    FileHandler(const char * inFileName, const char * outFileName): inFileName(inFileName), outFileName(outFileName){}
    ~FileHandler()
    {
        // Closing opened files
        inFile.close();
        outFile.close();
    }
    // This function opens both input and output streams and returns false in case of any errors
    bool Init()
    {
        // Try to open up the files via filestreams
        inFile.open(inFileName, ios::in | ios::binary);
        outFile.open(outFileName, ios::out);
        // Eof, failbit, badbit
        return(inFile.good() && outFile.good());
    }
    // This function reads data from previously opened file encoded in Huffman code and creates tree structure
    void LoadSerializedTree(TreeNode * root)
    {
        // Read '1' from stream
        if(ReadBit())
        {
            // Leaf encoutered. Read next 8bits of character and save it as symbol
            root->SetSymbol(ReadChar());
            return;
        }
        // Read '0' from stream
        else
        {
            // This node will be parent. Call recursively on leftChild and rightChild (pre-order serialization)
            root->CreateLeftChild();
            LoadSerializedTree(root->GetLeftChild());
            root->CreateRightChild();
            LoadSerializedTree(root->GetRightChild());
        }
    }
    // This function reads one bit from input stream. Returns 0 or 1
    // Source of idea: https://stackoverflow.com/questions/22390641/read-file-by-bits-c?fbclid=IwAR1hj0ShNBm1wErvJsRtd_hgXT3F3BhnEnbXRHqT4XIRb-8JrRQKMFqJFEE
    int ReadBit()
    {
        // Need to read another byte from stream. Either first, or end reading the last one
        if(currentBit < 0)
        {
            currentBit = 7;
            currentByte = inFile.get();
        }
        // Obtain next bit value (0/1) by bit shifting to right of current byte
        return ((currentByte >> currentBit--) & 1);   
    }
    // This function decompress chunks from input stream and saves them to output stream
    bool DecompressChunk(TreeNode * root, bool lastChunk)
    {
        // Calculate, how many compressed characters should be decompressed from actual chunk
        int compressedCharacterCount = lastChunk ? ReadLastChunkSize() : 4096;
      
        // Decompress N-characters
        for (int i = 0; i < compressedCharacterCount; i++)
        {
            // EOF should not be encountered
            if(inFile.eof() || outFile.bad())
            {
                return false;
            }
            // Obtain next decompressed character from input stream & write it to output stream
            char tmp = DecompressCharacter(root);
            outFile.put(tmp);
        }
        return true;
    }
};

bool decompressFile(const char * inFileName, const char * outFileName)
{
    // Important object declaration. Future initialization needed
    FileHandler fileHandler(inFileName, outFileName);
    TreeNode * root = new TreeNode();

    // Unable to open files or no permission to read or write into input/output files
    if(!fileHandler.Init())
    {
        delete root;
        return false;
    }
    // Assembling Huffman style tree previously serialized in input file
    fileHandler.LoadSerializedTree(root);

    // Print tree for debug
    // root->PrintTree(0);

    // Reading chunks 4096 bits long until first bit between chunks is 0. That indicates, that we have last chunk left
    while(fileHandler.ReadBit())
    {
        if(!fileHandler.DecompressChunk(root, false))
        {
            delete root;
            return false;
        }
    }
    // Last chunk encountered
    if(!fileHandler.DecompressChunk(root, true))
    {
        delete root;
        return false;
    }
    // Clean up
    delete root;
    
    return true;
}

bool compressFile ( const char * inFileName, const char * outFileName )
{
    // keep this dummy implementation (no bonus) or implement the compression (bonus)
    return false;
}
#ifndef __PROGTEST__
bool identicalFiles ( const char * fileName1, const char * fileName2 )
{
    // Source: https://stackoverflow.com/questions/6163611/compare-two-files/6163627
    // Used only for testing purposes...
    std::ifstream f1(fileName1, std::ifstream::binary | std::ifstream::ate);
    std::ifstream f2(fileName2, std::ifstream::binary | std::ifstream::ate);

    // File problem
    if (f1.fail() || f2.fail())
    {
        return false;
    }
    // Size mismatch
    if (f1.tellg() != f2.tellg())
    {
        return false;
    }
    // Seek back to beginning and use std::equal to compare contents
    f1.seekg(0, std::ifstream::beg);
    f2.seekg(0, std::ifstream::beg);
    return std::equal(
        std::istreambuf_iterator<char>(f1.rdbuf()),
        std::istreambuf_iterator<char>(),
        std::istreambuf_iterator<char>(f2.rdbuf())
    );
}

int main ( void )
{
    assert ( decompressFile ( "tests/test0.huf", "tempfile" ) );
    assert ( identicalFiles ( "tests/test0.orig", "tempfile" ) );

    assert ( decompressFile ( "tests/test1.huf", "tempfile" ) );
    assert ( identicalFiles ( "tests/test1.orig", "tempfile" ) );

    assert ( decompressFile ( "tests/test2.huf", "tempfile" ) );
    assert ( identicalFiles ( "tests/test2.orig", "tempfile" ) );

    assert ( decompressFile ( "tests/test3.huf", "tempfile" ) );
    assert ( identicalFiles ( "tests/test3.orig", "tempfile" ) );

    assert ( decompressFile ( "tests/test4.huf", "tempfile" ) );
    assert ( identicalFiles ( "tests/test4.orig", "tempfile" ) );

    assert ( ! decompressFile ( "tests/test5.huf", "tempfile" ) );


    // assert ( decompressFile ( "tests/extra0.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra0.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra1.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra1.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra2.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra2.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra3.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra3.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra4.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra4.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra5.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra5.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra6.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra6.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra7.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra7.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra8.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra8.orig", "tempfile" ) );

    // assert ( decompressFile ( "tests/extra9.huf", "tempfile" ) );
    // assert ( identicalFiles ( "tests/extra9.orig", "tempfile" ) );

    return 0;
}
#endif /* __PROGTEST__ */
