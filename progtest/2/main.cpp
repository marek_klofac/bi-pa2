#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <stdexcept>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */

class CPerson
{
private:
    string name;
    string address;
    string account;
    int sumIncome = 0;
    int sumExpense = 0;

public:
    CPerson(string name, string address, string account): name(name), address(address), account(account){}
    CPerson(string name, string address): name(name), address(address){}
    CPerson(string account): account(account){}
    string GetName() const
    {
        return name;
    }
    string GetAddress() const
    {
        return address;
    }
    string GetAccount() const
    {
        return account;
    }
    void SetAccount(string acc)
    {
        account = acc;
    }
    void Income(int amount)
    {
        sumIncome += amount;
    }
    void Expense(int amount)
    {
        sumExpense += amount;
    }
    int GetSumIncome()
    {
        return sumIncome;
    }
    int GetSumExpense()
    {
        return sumExpense;
    }
};

class CIterator
{
private:
    vector<CPerson *> data;
    unsigned int currentElem;

public:
    CIterator(){}
    CIterator(vector<CPerson *> data): data(data), currentElem(0){}
    bool AtEnd(void) const
    {
        return(currentElem == data.size());
    }
    void Next(void)
    {
        currentElem++;
    }
    string Name(void) const
    {
        return(data.at(currentElem)->GetName());
    }
    string Addr(void) const
    {
        return(data.at(currentElem)->GetAddress());
    }
    string Account(void) const
    {
        return(data.at(currentElem)->GetAccount());
    }
};

class CTaxRegister
{
private:
    // These two pointer vectors are always the same lenght and point to same CPerson objects
    // The only difference is they are differently sorted upon criteriums
    vector<CPerson *> sortedByName;
    vector<CPerson *> sortedByAccount;

    // Compare function for ordering by account
    bool static AccountCmp(const CPerson * a, const CPerson * b)
    {
        return(a->GetAccount().compare(b->GetAccount()) < 0);
    }
    // Compare function for ordering by name and if same then by address
    bool static NameAddressCmp(const CPerson * a, const CPerson * b)
    {
        if(a->GetName() == b->GetName())
        {
            return(a->GetAddress().compare(b->GetAddress()) < 0);
        }
        else
        {
            return(a->GetName().compare(b->GetName()) < 0);
        }
    }

public:
    void Print() const
    { 
        cout << "Sorted by names:" << endl;
        for(auto & person: sortedByName)
        {
            cout << person->GetName() << " | " << person->GetAddress() << " | " << person->GetAccount() << endl;
        }
        cout << "Sorted by accounts:" << endl;
        for(auto & person: sortedByAccount)
        {
            cout << person->GetName() << " | " << person->GetAddress() << " | " << person->GetAccount() << endl;
        }
    }
    bool Birth(const string & name, const string & addr, const string & account)
    {
        CPerson * newPerson = new CPerson(name, addr, account);

        // Person already existed in some of the pointer vectors
        if(binary_search(sortedByName.begin(), sortedByName.end(), newPerson, NameAddressCmp)
        || binary_search(sortedByAccount.begin(), sortedByAccount.end(), newPerson, AccountCmp))
        {
            cout << "Duplicate accounts" << endl;
            delete newPerson;
            return false;
        }
        // Searching in O(log n) time for appropriate insert positions
        auto sortedByNamePos = lower_bound(sortedByName.begin(), sortedByName.end(), newPerson, NameAddressCmp);
        auto sortedByAccountPos = lower_bound(sortedByAccount.begin(), sortedByAccount.end(), newPerson, AccountCmp);
        
        // Insert
        sortedByName.insert(sortedByNamePos, newPerson);
        sortedByAccount.insert(sortedByAccountPos, newPerson);

        return true;
    }
    bool Death(const string & name, const string & addr)
    {
        // Lookup person to be deleted via name and address
        CPerson * tmp = new CPerson(name, addr);

        // Deleting something, that doesn't exist
        if(!binary_search(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp))
        {
            cout << "Account was not found while deleting" << endl;
            delete tmp;
            return false;
        }
        // Searcing in O(log n) time for appropriate delete positions
        auto sortedByNamePos = lower_bound(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp);

        // Update tmp's account for later search
        tmp->SetAccount((*sortedByNamePos)->GetAccount());
        auto sortedByAccountPos = lower_bound(sortedByAccount.begin(), sortedByAccount.end(), tmp, AccountCmp);

        // Delete allocated CPerson object, that both pointers point to
        delete *sortedByNamePos;

        // Delete pointers from vectors
        sortedByName.erase(sortedByNamePos);
        sortedByAccount.erase(sortedByAccountPos);

        delete tmp;
        return true;
    }
    bool Income(const string & account, int amount)
    {
        CPerson * tmp = new CPerson(account);

        // Check if person exists
        if(!binary_search(sortedByAccount.begin(), sortedByAccount.end(), tmp, AccountCmp))
        {
            delete tmp;
            return false;
        }
        // Find person by it's account
        auto person = lower_bound(sortedByAccount.begin(), sortedByAccount.end(), tmp, AccountCmp);
        if(person == sortedByAccount.end())
        {
            delete tmp;
            return false;
        }
        (*person)->Income(amount);
        delete tmp;
        return true;
    }
    bool Income(const string & name, const string & addr, int amount)
    {
        CPerson * tmp = new CPerson(name, addr);

        // Check if person exists
        if(!binary_search(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp))
        {
            delete tmp;
            return false;
        }
        // Find person by it's name and address
        auto person = lower_bound(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp);
        if(person == sortedByName.end())
        {
            delete tmp;
            return false;
        }
        (*person)->Income(amount);
        delete tmp;
        return true;
    }
    bool Expense(const string & account, int amount)
    {
        CPerson * tmp = new CPerson(account);

        // Check if person exists
        if(!binary_search(sortedByAccount.begin(), sortedByAccount.end(), tmp, AccountCmp))
        {
            delete tmp;
            return false;
        }
        // Find person by it's account
        auto person = lower_bound(sortedByAccount.begin(), sortedByAccount.end(), tmp, AccountCmp);
        if(person == sortedByAccount.end())
        {
            delete tmp;
            return false;
        }
        (*person)->Expense(amount);
        delete tmp;
        return true;
    }
    bool Expense(const string & name, const string & addr, int amount)
    {
        CPerson * tmp = new CPerson(name, addr);

        // Check if person exists
        if(!binary_search(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp))
        {
            delete tmp;
            return false;
        }
        // Find person by it's name and address
        auto person = lower_bound(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp);
        (*person)->Expense(amount);
        delete tmp;
        return true;
    }    
    bool Audit(const string & name, const string & addr, string & account, int & sumIncome, int & sumExpense) const
    {
        CPerson * tmp = new CPerson(name, addr);

         // Check if person exists
        if(!binary_search(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp))
        {
            delete tmp;
            return false;
        }
        // Find person by it's name and address
        auto person = lower_bound(sortedByName.begin(), sortedByName.end(), tmp, NameAddressCmp);
        account = (*person)->GetAccount();
        sumIncome = (*person)->GetSumIncome();
        sumExpense = (*person)->GetSumExpense();

        delete tmp;
        return true;
    }
    CIterator ListByName(void) const
    {
        return CIterator(sortedByName);
    }
    
    // Destructor, freeing up allocated resources
    ~CTaxRegister()
    {
        for(auto & person: sortedByName)
        {
            delete person;
        }
    }
};

#ifndef __PROGTEST__
int main ( void )
{
    string acct;
    int    sumIncome, sumExpense;
    CTaxRegister b1;
    assert ( b1 . Birth ( "John Smith", "Oak Road 23", "123/456/789" ) );
    assert ( b1 . Birth ( "Jane Hacker", "Main Street 17", "Xuj5#94" ) );
    assert ( b1 . Birth ( "Peter Hacker", "Main Street 17", "634oddT" ) );
    assert ( b1 . Birth ( "John Smith", "Main Street 17", "Z343rwZ" ) );
    assert ( b1 . Income ( "Xuj5#94", 1000 ) );
    assert ( b1 . Income ( "634oddT", 2000 ) );
    assert ( b1 . Income ( "123/456/789", 3000 ) );
    assert ( b1 . Income ( "634oddT", 4000 ) );
    assert ( b1 . Income ( "Peter Hacker", "Main Street 17", 2000 ) );
    assert ( b1 . Expense ( "Jane Hacker", "Main Street 17", 2000 ) );
    assert ( b1 . Expense ( "John Smith", "Main Street 17", 500 ) );
    assert ( b1 . Expense ( "Jane Hacker", "Main Street 17", 1000 ) );
    assert ( b1 . Expense ( "Xuj5#94", 1300 ) );
    assert ( b1 . Audit ( "John Smith", "Oak Road 23", acct, sumIncome, sumExpense ) );
    assert ( acct == "123/456/789" );
    assert ( sumIncome == 3000 );
    assert ( sumExpense == 0 );
    assert ( b1 . Audit ( "Jane Hacker", "Main Street 17", acct, sumIncome, sumExpense ) );
    assert ( acct == "Xuj5#94" );
    assert ( sumIncome == 1000 );
    assert ( sumExpense == 4300 );
    assert ( b1 . Audit ( "Peter Hacker", "Main Street 17", acct, sumIncome, sumExpense ) );
    assert ( acct == "634oddT" );
    assert ( sumIncome == 8000 );
    assert ( sumExpense == 0 );
    assert ( b1 . Audit ( "John Smith", "Main Street 17", acct, sumIncome, sumExpense ) );
    assert ( acct == "Z343rwZ" );
    assert ( sumIncome == 0 );
    assert ( sumExpense == 500 );
    CIterator it = b1 . ListByName ();
    assert ( ! it . AtEnd ()
            && it . Name () == "Jane Hacker"
            && it . Addr () == "Main Street 17"
            && it . Account () == "Xuj5#94" );
    it . Next ();
    assert ( ! it . AtEnd ()
            && it . Name () == "John Smith"
            && it . Addr () == "Main Street 17"
            && it . Account () == "Z343rwZ" );
    it . Next ();
    assert ( ! it . AtEnd ()
            && it . Name () == "John Smith"
            && it . Addr () == "Oak Road 23"
            && it . Account () == "123/456/789" );
    it . Next ();
    assert ( ! it . AtEnd ()
            && it . Name () == "Peter Hacker"
            && it . Addr () == "Main Street 17"
            && it . Account () == "634oddT" );
    it . Next ();
    assert ( it . AtEnd () );

    assert ( b1 . Death ( "John Smith", "Main Street 17" ) );

    CTaxRegister b2;
    assert ( b2 . Birth ( "John Smith", "Oak Road 23", "123/456/789" ) );
    assert ( b2 . Birth ( "Jane Hacker", "Main Street 17", "Xuj5#94" ) );
    assert ( !b2 . Income ( "634oddT", 4000 ) );
    assert ( !b2 . Expense ( "John Smith", "Main Street 18", 500 ) );
    assert ( !b2 . Audit ( "John Nowak", "Second Street 23", acct, sumIncome, sumExpense ) );
    assert ( !b2 . Death ( "Peter Nowak", "5-th Avenue" ) );
    assert ( !b2 . Birth ( "Jane Hacker", "Main Street 17", "4et689A" ) );
    assert ( !b2 . Birth ( "Joe Hacker", "Elm Street 23", "Xuj5#94" ) );
    assert ( b2 . Death ( "Jane Hacker", "Main Street 17" ) );
    assert ( b2 . Birth ( "Joe Hacker", "Elm Street 23", "Xuj5#94" ) );
    assert ( b2 . Audit ( "Joe Hacker", "Elm Street 23", acct, sumIncome, sumExpense ) );
    assert ( acct == "Xuj5#94" );
    assert ( sumIncome == 0 );
    assert ( sumExpense == 0 );
    assert ( !b2 . Birth ( "Joe Hacker", "Elm Street 23", "AAj5#94" ) );

    return 0;
}
#endif /* __PROGTEST__ */
