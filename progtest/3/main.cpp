#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

class CBigInt
{
private:
    bool isNegative;
    string data;

public:
    CBigInt(): isNegative(false), data("0"){}

    CBigInt(int number): CBigInt(to_string(number)){}

    CBigInt(const char * number): CBigInt(string(number)){}

    CBigInt(const string & number)
    {
        isNegative = false;
        // Checking validity of string obtained
        for(auto & digit: number)
        {
            // Skip leading whitespaces and leading zeros
            if(isspace(digit) || digit == 0)
            {
                continue;
            }
            // Check sign
            if(digit == '-')
            {
                this->isNegative = true;
                continue;
            }
            // Skip whitespaces after sign
            if(isspace(digit) || digit == 0)
            {
                continue;
            }
            // Get the rest of numbers
            if(!isdigit(digit))
            {
                throw invalid_argument("String containing other characters than numbers.");
            }
            this->data.push_back(digit);
        }
        if(this->data == "0")
        {
            this->isNegative = false;
        }
    }
    // operator +=, any of {CBigInt/int/string}
    // operator *=, any of {CBigInt/int/string}

    // Comparing absolute values, no sign
    bool IsAbsLower(const CBigInt & other) const
    { 
        // Calculate lengths of both string
        int n1 = this->data.length(), n2 = other.data.length();
        if(n1 < n2)
            return true;
        if(n2 < n1)
            return false;
        for(int i = 0; i < n1; i++)
        {
            if(this->data[i] < other.data[i])
            {
                return true;
            }
            else if(this->data[i] > other.data[i])
            {
                return false;
            }
        }
        return false; 
    }
    // Takes into account the sign of number
    bool IsLower(const CBigInt & other) const
    {
        // - + or + -
        if(this->isNegative ^ other.isNegative)
        {
            return(this->isNegative);
        }
        // + + or - -
        return(this->IsAbsLower(other));
    }

    CBigInt & operator += (const CBigInt & other)
    {
        *this = *this + other;
        return(*this);
    }
    CBigInt & operator *= (const CBigInt & other)
    {
        *this = *this * other;
        return(*this);
    }
    friend CBigInt operator + (const CBigInt & lhs, const CBigInt & rhs);
    friend CBigInt operator * (const CBigInt & lhs, const CBigInt & rhs);
    friend bool operator < (const CBigInt & lhs, const CBigInt & rhs);
    friend bool operator <= (const CBigInt & lhs, const CBigInt & rhs);
    friend bool operator > (const CBigInt & lhs, const CBigInt & rhs);
    friend bool operator >= (const CBigInt & lhs, const CBigInt & rhs);
    friend bool operator == (const CBigInt & lhs, const CBigInt & rhs);
    friend bool operator != (const CBigInt & lhs, const CBigInt & rhs);
    friend ostream & operator << (ostream & os, const CBigInt & x);
    friend istream & operator >> (istream & is, CBigInt & x);
  
};

// Modified and inspired -> Source: https://www.geeksforgeeks.org/sum-two-large-numbers/
//                       -> Source: https://www.geeksforgeeks.org/difference-of-two-large-numbers/
CBigInt operator + (const CBigInt & lhs, const CBigInt & rhs)
{
    CBigInt result("");
    CBigInt lowerNumber = lhs;
    CBigInt higherNumber = rhs;

    // - - and + + cases
    // -100 + (-20) == -120
    //  100 +   20  ==  120
    
    // - + and + - cases
    // -100 +   20  ==  -80
    //  100 + (-20) ==   80

    // Don't take sign into account. Only length and digits comparation
    // -100 and 20 --> 20 would be lower number
    if(higherNumber.IsAbsLower(lowerNumber))
    {
        swap(lowerNumber, higherNumber);
    }
    // Prepare backward iterators
    auto lo_it = lowerNumber.data.crbegin();
    auto hi_it = higherNumber.data.crbegin();
    bool carry = false;

    // - + and + - cases
    // Subtract absolute values, solve sign later
    if(lowerNumber.isNegative ^ higherNumber.isNegative)
    {
        // First iterating over shorter number
        while(lo_it != lowerNumber.data.crend())
        {   
            int sub = ((*hi_it - '0') - (*lo_it - '0') - carry);
            if (sub < 0) 
            { 
                sub = sub + 10; 
                carry = 1; 
            }
            else
            {
                carry = 0; 
            }
            result.data.push_back(sub + '0');
            lo_it++;
            hi_it++;
        }
        // Iterate over the rest of longer number
        while(hi_it != higherNumber.data.crend())
        {
            int sub = ((*hi_it - '0') - carry);
            if (sub < 0)
            { 
                sub = sub + 10; 
                carry = 1; 
            }
            else
            {
                carry = 0; 
            }              
            result.data.push_back(sub + '0'); 
            hi_it++;
        }
        // Solve sign
        result.isNegative = higherNumber.isNegative ? true : false;
    }
    // - - and + + cases
    // Add together absolute values, solve sign later
    else
    {
        // First iterating over shorter number
        while(lo_it != lowerNumber.data.crend())
        {   
            int sum = ((*lo_it - '0') + (*hi_it - '0') + carry); 
            result.data.push_back(sum % 10 + '0');
            carry = sum / 10;
            lo_it++;
            hi_it++;
        }
        // Iterate over the rest of longer number
        while(hi_it != higherNumber.data.crend())
        {
            int sum = ((*hi_it - '0') + carry);
            result.data.push_back(sum % 10 + '0'); 
            carry = sum / 10;
            hi_it++;
        }
        // Solve carry
        if(carry)
        {
            result.data.push_back(carry + '0'); 
        }
        // Solve sign
        result.isNegative = (lowerNumber.isNegative && higherNumber.isNegative) ? true : false;
    }
    reverse(result.data.begin(), result.data.end());
    if(result.data == "0")
    {
        result.isNegative = false;
    }
    return result;
}
// Source (modified): https://www.geeksforgeeks.org/multiply-large-numbers-represented-as-strings/
CBigInt operator * (const CBigInt & lhs, const CBigInt & rhs)
{
    if(lhs == 0 || rhs == 0)
    {
        return CBigInt(0);
    }
    CBigInt result(string(lhs.data.size() + rhs.data.size(), '0'));
    CBigInt lowerNumber = lhs;
    CBigInt higherNumber = rhs;

    if(higherNumber.IsAbsLower(lowerNumber))
    {
        swap(lowerNumber, higherNumber);
    }
    // Prepare iterators
    auto lo_it = lowerNumber.data.crbegin();
    auto hi_it = higherNumber.data.crbegin();
    int res1_it = 0;
    int res2_it = 0;

    while(lo_it != lowerNumber.data.crend())
    {
        // Reset values for next iteration
        int carry = 0;
        res2_it = 0;
        hi_it = higherNumber.data.crbegin();
        while(hi_it != higherNumber.data.crend())
        {
            int sum = ((*lo_it - '0') * (*hi_it - '0')) + (result.data[res1_it + res2_it] - '0') + carry;
            carry = sum / 10;
            result.data[res1_it + res2_it] = sum % 10 + '0';
            res2_it++;
            hi_it++;
        }
        if(carry > 0)
        {
            result.data[res1_it + res2_it] += carry;
        }
        res1_it++;
        lo_it++;
    }
    reverse(result.data.begin(), result.data.end());
    // Solve sign
    if(lowerNumber.isNegative ^ higherNumber.isNegative)
    {
        result.isNegative = true;
    }
    return result;
}
bool operator < (const CBigInt & lhs, const CBigInt & rhs)
{
    return(lhs.IsLower(rhs));
}
bool operator <= (const CBigInt & lhs, const CBigInt & rhs)
{
    return(lhs < rhs || lhs == rhs);
}
bool operator > (const CBigInt & lhs, const CBigInt & rhs)
{
    return(rhs.IsLower(lhs));
}
bool operator >= (const CBigInt & lhs, const CBigInt & rhs)
{
    return(lhs > rhs || lhs == rhs);
}
bool operator == (const CBigInt & lhs, const CBigInt & rhs)
{
    return((lhs.data == rhs.data) && (lhs.isNegative == rhs.isNegative));
}
bool operator != (const CBigInt & lhs, const CBigInt & rhs)
{
    return((lhs.data != rhs.data) || (lhs.isNegative != rhs.isNegative));
}
ostream & operator << (ostream & os, const CBigInt & x)
{
    if(x.isNegative)
    {
        os << '-';
    }
    for(auto & digit: x.data)
    {
        // Skip leading zeros
        if(x.data[0] == '0' && x.data.size() > 1)
        {
            continue;
        }
        os << digit;
    }
    return os;
}
istream & operator >> (istream & is, CBigInt & x)
{
    char readChar;
    CBigInt tmp;
    tmp.data.clear();

    // Solve sign
    is >> readChar;
    if(readChar == '-')
    {
        tmp.isNegative = true;
    }
    else
    {
        is.putback(readChar);
    }
    // Load number
    while(isdigit(is.peek()))
    {
        // Get char from stream
        is.get(readChar);

        // Skip leading zeros
        if(readChar == '0' && tmp.data.empty())
        {
            continue;
        }
        tmp.data.push_back(readChar);
    }
    // If any data read, modify
    if(!tmp.data.empty())
    {
        x = tmp;
    }
    else
    {
        is.setstate(ios::failbit);
    }
    return is;
}

#ifndef __PROGTEST__
static bool equal ( const CBigInt & x, const char * val )
{
    ostringstream oss;
    oss << x;
    return oss . str () == val;
}
int main ( void )
{
    CBigInt a, b;
    istringstream is;

    is.str("11");
    is >> a;
    assert(equal(a, "11"));

    CBigInt c(12);
    assert(equal(c, "12"));

    assert(equal(c + "0005", "17"));
    assert(equal(c + a, "23"));
    assert(equal(c + 8, "20"));
    assert(equal(c + "432", "444"));
    assert(equal(10 + c, "22"));
    assert(equal("9" + c, "21"));

    CBigInt d(-8);
    assert(equal(d + -8, "-16"));
    assert(equal(c + -8, "4"));
    assert(equal(-2 + c, "10"));
    assert(equal("9" + d, "1"));
    assert(equal(d + "8", "0"));

    assert(equal(c * 0, "0"));
    assert(equal(0 * c, "0"));
    assert(equal(c * a, "132"));
    assert(equal(c * 8, "96"));
    assert(equal(c * "4", "48"));
    assert(equal(10 * c, "120"));
    assert(equal("9" * c, "108"));

    assert(equal(c += a, "23"));
    c = 12;
    assert(equal(c *= a, "132"));

    a = 10;
    a += 20;

    assert ( equal ( a, "30" ) );
    a *= 5;
    assert ( equal ( a, "150" ) );
    b = a + 3;
    assert ( equal ( b, "153" ) );
    b = a * 7;
    assert ( equal ( b, "1050" ) );
    assert ( equal ( a, "150" ) );

    a = 10;
    a += -20;
    assert ( equal ( a, "-10" ) );
    a *= 5;
    assert ( equal ( a, "-50" ) );
    b = a + 73;
    assert ( equal ( b, "23" ) );
    b = a * -7;
    assert ( equal ( b, "350" ) );
    assert ( equal ( a, "-50" ) );

    a = "12345678901234567890";
    a += "-99999999999999999999";
    assert ( equal ( a, "-87654321098765432109" ) );
    a *= "54321987654321987654";
    assert ( equal ( a, "-4761556948575111126880627366067073182286" ) );
    a *= 0;
    assert ( equal ( a, "0" ) );
    a = 10;
    b = a + "400";
    assert ( equal ( b, "410" ) );
    b = a * "15";
    assert ( equal ( b, "150" ) );
    assert ( equal ( a, "10" ) );

    is . clear ();
    is . str ( " 1234" );
    assert ( is >> b );
    assert ( equal ( b, "1234" ) );
    is . clear ();
    is . str ( " 12 34" );
    assert ( is >> b );
    assert ( equal ( b, "12" ) );
    is . clear ();
    is . str ( "999z" );
    assert ( is >> b );
    assert ( equal ( b, "999" ) );
    is . clear ();
    is . str ( "abcd" );
    assert ( ! ( is >> b ) );
    is . clear ();
    is . str ( "- 758" );
    assert ( ! ( is >> b ) );
    try
    {
    a = "-xyz";
    assert ( "missing an exception" == NULL );
    }
    catch ( const invalid_argument & e )
    {
    }

    a = "73786976294838206464";
    assert ( a < "1361129467683753853853498429727072845824" );
    assert ( a <= "1361129467683753853853498429727072845824" );
    assert ( ! ( a > "1361129467683753853853498429727072845824" ) );
    assert ( ! ( a >= "1361129467683753853853498429727072845824" ) );
    assert ( ! ( a == "1361129467683753853853498429727072845824" ) );
    assert ( a != "1361129467683753853853498429727072845824" );
    assert ( ! ( a < "73786976294838206464" ) );
    assert ( a <= "73786976294838206464" );
    assert ( ! ( a > "73786976294838206464" ) );
    assert ( a >= "73786976294838206464" );
    assert ( a == "73786976294838206464" );
    assert ( ! ( a != "73786976294838206464" ) );
    assert ( a < "73786976294838206465" );
    assert ( a <= "73786976294838206465" );
    assert ( ! ( a > "73786976294838206465" ) );
    assert ( ! ( a >= "73786976294838206465" ) );
    assert ( ! ( a == "73786976294838206465" ) );
    assert ( a != "73786976294838206465" );
    a = "2147483648";
    assert ( ! ( a < -2147483648 ) );
    assert ( ! ( a <= -2147483648 ) );
    assert ( a > -2147483648 );
    assert ( a >= -2147483648 );
    assert ( ! ( a == -2147483648 ) );
    assert ( a != -2147483648 );

    return 0;
}
#endif /* __PROGTEST__ */
