#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;
#endif /* __PROGTEST__ */

const int DEFAULT_ARR_LEN = 10;
class Transaction;

template <typename T>
class Buffer 
{
private:
    T * array;
    unsigned int elemCount;
    unsigned int arrLength;

public:
    Buffer(): elemCount(0), arrLength(DEFAULT_ARR_LEN)
    {
        array = new T[arrLength];
    }
    ~Buffer()
    {
        delete [] array;
    }
    Buffer & operator =(const Buffer & src)
    {
        //cout << "ASSIGN" << endl;
        if(this == &src)
        {
            return *this;
        }
        elemCount = src.elemCount;
        arrLength = src.arrLength;
        delete [] array;
        array = new T[arrLength];
        for (unsigned int i = 0; i < elemCount; i++)
        {
            array[i] = src.array[i];
        }
        return *this;
    }
    Buffer(const Buffer & src)
    {
        //cout << "COPY" << endl;
        elemCount = src.elemCount;
        arrLength = src.arrLength;
        array = new T[arrLength];
        for (unsigned int i = 0; i < elemCount; i++)
        {
            array[i] = src.array[i];
        }
    }
    void push_back(const T & elem)
    {
        // Reallocate in O(n) time
        if(elemCount == arrLength)
        {
            arrLength *= 2;
            T * tmp = new T[arrLength];
            for(unsigned int i = 0; i < elemCount; i++)
            {
                tmp[i] = array[i];
            }
            delete [] array;
            array = tmp;
        }
        // Append in O(1) time
        array[elemCount++] = elem;
    }
    void erase(unsigned int i)
    {
        array[i] = array[elemCount - 1];
        elemCount--;
    }
    void clear()
    {
        delete [] array;
        elemCount = 0;
        arrLength = DEFAULT_ARR_LEN;
        array = new T[arrLength];
    }
    unsigned int size() const
    {
        return elemCount;
    }
    bool empty() const
    {
        return(elemCount == 0);
    }
    void print() const
    {
        for(unsigned int i = 0; i < elemCount; i++)
        {
            cout << array[i] << " ";
        }
        cout << endl;
    }
    T * data()
    {
        return array;
    }
    T & operator [](unsigned int i) const
    {
        return array[i];
    }
    friend ostream & operator <<(ostream & os, const Buffer<char> & buffer);
    friend ostream & operator <<(ostream & os, const Buffer<Transaction> & buffer);
};

class Transaction
{
private:
    Buffer<char> source;
    Buffer<char> destination;
    int amount;
    Buffer<char> sign;
    bool income;

public:
    Transaction(){}
    Transaction(const char * debAccID, const char * credAccID, unsigned int amount, const char * signature, bool income):
        amount(amount),
        income(income)
    {
        unsigned int len = strlen(debAccID);
        for (unsigned int i = 0; i < len; i++)
        {
            source.push_back(debAccID[i]);
        }
        source.push_back('\0');
        len = strlen(credAccID);
        for (unsigned int i = 0; i < len; i++)
        {
            destination.push_back(credAccID[i]);
        }
        destination.push_back('\0');
        len = strlen(signature);
        for (unsigned int i = 0; i < len; i++)
        {
            sign.push_back(signature[i]);
        }
        sign.push_back('\0');
    }
    ~Transaction(){}
    Transaction & operator =(const Transaction & src)
    {
        if(this == &src)
        {
            return *this;
        }
        source = src.source;
        destination = src.destination;
        amount = src.amount;
        sign = src.sign;
        income = src.income;
        return *this;
    }
    Transaction(const Transaction & src):
        source(src.source),
        destination(src.destination),
        amount(src.amount),
        sign(src.sign),
        income(src.income)
    {}
    friend ostream & operator <<(ostream & os, const Transaction & transaction);
};

class Acc
{
private:
    Buffer<char> id;
    Buffer<Transaction> transactions;
    int initBalance;
    int finalBalance;

public:
    Acc():initBalance(0), finalBalance(0){}
    Acc(const char * accID, int initBalance):
        initBalance(initBalance),
        finalBalance(initBalance)
    {
        unsigned int len = strlen(accID);
        for (unsigned int i = 0; i < len; i++)
        {
            id.push_back(accID[i]);
        }
        id.push_back('\0');
    }
    ~Acc(){}
    Acc & operator =(const Acc & src)
    {
        if(this == &src)
        {
            return *this;
        }
        id = src.id;
        transactions = src.transactions;
        initBalance = src.initBalance;
        finalBalance = src.finalBalance;
        return *this;
    }
    Acc(const Acc & src):
        id(src.id),
        transactions(src.transactions),
        initBalance(src.initBalance),
        finalBalance(src.finalBalance)
    {}
    // Returns final balance of an account
    int Balance()
    {
        return finalBalance;
    }
    const char * GetId()
    {
        return id.data();
    }
    void Trim()
    {
        transactions.clear();
        initBalance = finalBalance;
    }
    void Incoming(const char * debAccID, const char * credAccID, unsigned int amount, const char * signature)
    {
        finalBalance += amount;
        transactions.push_back(Transaction(debAccID, credAccID, amount, signature, true));
    }
    void Outgoing(const char * debAccID, const char * credAccID, unsigned int amount, const char * signature)
    {
        finalBalance -= amount;
        transactions.push_back(Transaction(debAccID, credAccID, amount, signature, false));
    }
    friend ostream & operator <<(ostream & os, const Acc & account);
};

class CBank
{
private:
    Buffer<Acc> accounts;

public:
    CBank(){}
    ~CBank()
    {

    }
    CBank & operator =(const CBank & src)
    {
        if(this == &src)
        {
            return *this;
        }
        accounts = src.accounts;
        return *this;
    }
    CBank(const CBank & src):
        accounts(src.accounts)
    {}
    // accID has to be unique! initialBalance can be negative or 0
    // returns false if account already exists
    bool NewAccount(const char * accID, int initBalance)
    {
        if(GetAccountIndex(accID) != accounts.size())
        {
            return false;
        }
        accounts.push_back(Acc(accID, initBalance));
        return true;
    }
    // debAcc - amount and credAcc + amount, amount can be 0
    // returns false if accounts don't exist or debAcc and credAcc are the same
    bool Transaction(const char * debAccID, const char * credAccID, unsigned int amount, const char * signature)
    {
        unsigned int debAccIdx = GetAccountIndex(debAccID);
        unsigned int credAccIdx = GetAccountIndex(credAccID);
        if(debAccIdx == accounts.size()
        || credAccIdx == accounts.size()
        || strcmp(debAccID, credAccID) == 0)
        {
            return false;
        }
        accounts[debAccIdx].Outgoing(debAccID, credAccID, amount, signature);
        accounts[credAccIdx].Incoming(debAccID, credAccID, amount, signature);
        return true;
    }
    // Deletes all transaction history of account. Make finalBalance as initBalance
    // and delete transaction buffer.
    // returns false if account doesn't exist 
    bool TrimAccount(const char * accID)
    {
        unsigned int idx = GetAccountIndex(accID);
        if(idx == accounts.size())
        {
            return false;
        }
        accounts[idx].Trim();
        return true;
    }
    // Return a reference to an account by given ID
    // if account doesn't exist, raise an exception
    Acc & Account(const char * accID)
    {
        unsigned int idx = GetAccountIndex(accID);
        if(idx == accounts.size())
        {
            throw invalid_argument("Account doesn't exist.");
        }
        return accounts[idx];
    }
    unsigned int GetAccountIndex(const char * accID)
    {
        for(unsigned int i = 0; i < accounts.size(); ++i)
        {
            if(strcmp(accounts[i].GetId(), accID) == 0)
            {
                return i;
            }
        }
        return accounts.size();
    }
};

// "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n"
ostream & operator <<(ostream & os, const Acc & account)
{
    os << account.id << ":" << endl;
    os << "   " << account.initBalance << endl;
    if(!account.transactions.empty())
    {
        os << account.transactions << endl;
    }
    os << " = " << account.finalBalance << endl;
    return os;
}
ostream & operator <<(ostream & os, const Buffer<char> & buffer)
{
    for (unsigned int i = 0; i < buffer.elemCount - 1; i++)
    {
        os << buffer[i];
    }
    return os;
}
ostream & operator <<(ostream & os, const Buffer<Transaction> & buffer)
{
    for (unsigned int i = 0; i < buffer.elemCount; i++)
    {
        os << buffer[i];
        // Last transaction, dont print \n
        if(i != buffer.elemCount - 1)
        {
            os << endl;
        }
    }
    return os;
}
ostream & operator <<(ostream & os, const Transaction & transaction)
{
    if(transaction.income)
    {
        os << " + ";
        os << transaction.amount << ", from: ";
        os << transaction.source << ", sign: ";
        os << transaction.sign;
    }
    else
    {
        os << " - ";
        os << transaction.amount << ", to: ";
        os << transaction.destination << ", sign: ";
        os << transaction.sign;
    }
    
    
    // os << "FROM: " << transaction.source << " | TO: " << transaction.destination << " | Amount: " << transaction.amount << " | Signature: " << transaction.sign << " | Income: " << transaction.income;
    return os;
}

#ifndef __PROGTEST__
int main ( void )
{
    // CBank a;
    // a.NewAccount("123", 1000);
    // a.NewAccount("abc", 2000);
    // a.Transaction("123", "abc", 100, "yolo");
    // a.Transaction("abc", "123", 30, "ne");
    // a.Transaction("123", "abc", 20, "test");
    // cout << a.Account("123") << endl;
    // cout << a.Account("abc") << endl;
    // a.TrimAccount("123");
    // cout << a.Account("123") << endl;

    ostringstream os;
    char accCpy[100], debCpy[100], credCpy[100], signCpy[100];
    CBank x0;
    assert ( x0 . NewAccount ( "123456", 1000 ) );
    assert ( x0 . NewAccount ( "987654", -500 ) );
    assert ( x0 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x0 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x0 . NewAccount ( "111111", 5000 ) );
    assert ( x0 . Transaction ( "111111", "987654", 290, "Okh6e+8rAiuT5=" ) );
    assert ( x0 . Account ( "123456" ). Balance ( ) ==  -2190 );
    assert ( x0 . Account ( "987654" ). Balance ( ) ==  2980 );
    assert ( x0 . Account ( "111111" ). Balance ( ) ==  4710 );
    os . str ( "" );
    os << x0 . Account ( "123456" );
    assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
    os . str ( "" );
    os << x0 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 290, from: 111111, sign: Okh6e+8rAiuT5=\n = 2980\n" ) );
    os . str ( "" );
    os << x0 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 290, to: 987654, sign: Okh6e+8rAiuT5=\n = 4710\n" ) );
    
    
    assert ( x0 . TrimAccount ( "987654" ) );
    assert ( x0 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
    os . str ( "" );
    os << x0 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   2980\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 3103\n" ) );

    CBank x2;
    strncpy ( accCpy, "123456", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, 1000 ));
    strncpy ( accCpy, "987654", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, -500 ));
    strncpy ( debCpy, "123456", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "XAbG5uKz6E=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 300, signCpy ) );
    strncpy ( debCpy, "123456", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "AbG5uKz6E=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
    strncpy ( accCpy, "111111", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, 5000 ));
    strncpy ( debCpy, "111111", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "Okh6e+8rAiuT5=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
    assert ( x2 . Account ( "123456" ). Balance ( ) ==  -2190 );
    assert ( x2 . Account ( "987654" ). Balance ( ) ==  5580 );
    assert ( x2 . Account ( "111111" ). Balance ( ) ==  2110 );
    os . str ( "" );
    os << x2 . Account ( "123456" );
    assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
    os . str ( "" );
    os << x2 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n = 5580\n" ) );
    os . str ( "" );
    os << x2 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n = 2110\n" ) );
    assert ( x2 . TrimAccount ( "987654" ) );
    strncpy ( debCpy, "111111", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "asdf78wrnASDT3W", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 123, signCpy ) );
    os . str ( "" );
    os << x2 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   5580\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );

    CBank x4;
    assert ( x4 . NewAccount ( "123456", 1000 ) );
    assert ( x4 . NewAccount ( "987654", -500 ) );
    assert ( !x4 . NewAccount ( "123456", 3000 ) );
    assert ( !x4 . Transaction ( "123456", "666", 100, "123nr6dfqkwbv5" ) );
    assert ( !x4 . Transaction ( "666", "123456", 100, "34dGD74JsdfKGH" ) );
    assert ( !x4 . Transaction ( "123456", "123456", 100, "Juaw7Jasdkjb5" ) );
    try
    {
    x4 . Account ( "666" ). Balance ( );
    assert ( "Missing exception !!" == NULL );
    }
    catch ( ... )
    {
    }
    try
    {
    os << x4 . Account ( "666" ). Balance ( );
    assert ( "Missing exception !!" == NULL );
    }
    catch ( ... )
    {
    }
    assert ( !x4 . TrimAccount ( "666" ) );

    CBank x6;
    assert ( x6 . NewAccount ( "123456", 1000 ) );
    assert ( x6 . NewAccount ( "987654", -500 ) );
    assert ( x6 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x6 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x6 . NewAccount ( "111111", 5000 ) );
    assert ( x6 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
    CBank x7 ( x6 );
    assert ( x6 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
    assert ( x7 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
    assert ( x6 . NewAccount ( "99999999", 7000 ) );
    assert ( x6 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
    assert ( x6 . TrimAccount ( "111111" ) );
    assert ( x6 . Transaction ( "123456", "111111", 221, "Q23wr234ER==" ) );
    os . str ( "" );
    os << x6 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n + 221, from: 123456, sign: Q23wr234ER==\n = -1581\n" ) );
    os . str ( "" );
    os << x6 . Account ( "99999999" );
    assert ( ! strcmp ( os . str () . c_str (), "99999999:\n   7000\n + 3789, from: 111111, sign: aher5asdVsAD\n = 10789\n" ) );
    os . str ( "" );
    os << x6 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );
    os . str ( "" );
    os << x7 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );
    try
    {
    os << x7 . Account ( "99999999" ). Balance ( );
    assert ( "Missing exception !!" == NULL );
    }
    catch ( ... )
    {
    }
    os . str ( "" );
    os << x7 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 789, from: 111111, sign: SGDFTYE3sdfsd3W\n = 6369\n" ) );

    CBank x8;
    CBank x9;
    assert ( x8 . NewAccount ( "123456", 1000 ) );
    assert ( x8 . NewAccount ( "987654", -500 ) );
    assert ( x8 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x8 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x8 . NewAccount ( "111111", 5000 ) );
    assert ( x8 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
    x9 = x8;
    assert ( x8 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
    assert ( x9 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
    assert ( x8 . NewAccount ( "99999999", 7000 ) );
    assert ( x8 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
    assert ( x8 . TrimAccount ( "111111" ) );
    os . str ( "" );
    os << x8 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n = -1802\n" ) );
    os . str ( "" );
    os << x9 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );

    return 0;
}
#endif /* __PROGTEST__ */
