#include "Bag.hpp"

Bag::Bag(){}

Bag::Bag(int size): size(size), count(0)
{
    array = new int[size];
}
Bag::Bag(const Bag & src): count(src.count), size(src.size)
{
    array = new int[size];
    for (int i = 0; i < count; i++)
    {
        array[i] = src.array[i];
    }
}
Bag::~Bag()
{
    delete [] array;
}
void Bag::insert(const int & x)
{
    // Resize of array
    if(count == size)
    {
        int * tmp = new int[size * 2];
        for (int i = 0; i < count; i++)
        {
            tmp[i] = array[i];
        }
        delete [] array;
        array = tmp;
        size *= 2;
    }
    array[count++] = x;
}
void Bag::remove(const int & x)
{
    for (int i = 0; i < count; i++)
    {
        if(array[i] == x)
        {
            array[i] = array[count-1];
            count--;
            return;
        }
    }
    
}
bool Bag::exist(const int & x)
{
    for (int i = 0; i < count; i++)
    {
        if(array[i] == x)
        {
            return true;
        }
    }
    return false;
}
const Bag & Bag::operator = (const Bag & src)
{
    if(this == &src)
    {
        return *this;
    }
    delete [] array;
    array = new int[size];
    for (int i = 0; i < count; i++)
    {
        array[i] = src.array[i];
    }
    return *this;
}
void Bag::operator += (const int & x)
{
    insert(x);
}
ostream & operator << (ostream & os, const Bag & bag)
{
    for (int i = 0; i < bag.count; i++)
    {
        os << bag.array[i] << " ";
    }
    return os;
}