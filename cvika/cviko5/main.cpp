#include "Bag.hpp"
using namespace std;

int main() {
  Bag b1;
  b1.insert(1);
  b1.insert(3);
  b1.insert(1);
  b1.insert(2);
  cout << "b1 = " << b1 << endl;
  // b1 = 1, 3, 1, 2  
  
  Bag b2 = b1;
  b1.remove(1);
  cout << "b1 = " << b1 << endl;
  // b1 = 2, 3, 1  
  cout << "b2 = " << b2 << endl;
  // b2 = 1, 3, 1, 2  
  
  Bag b3;
  Bag b4; 
  b4 = b3 = b1;
  b1.remove(1);
  b1.insert(7);
  b3+=4;
  b3.insert(42);
   
  cout<<b1.exist(4)<<endl;
  // 0
  cout<<b3.exist(4)<<endl;
  // 1
  
  cout << "b1 = " << b1 << endl;
  // 2, 3, 7
  cout << "b3 = " << b3 << endl;
  // 2, 3, 1, 4, 42
  cout << "b4 = " << b4 << endl;
  // 2, 3, 1 
   
  return 0;
}
