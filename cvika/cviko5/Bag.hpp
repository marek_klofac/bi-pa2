#include <iostream>

using namespace std;

class Bag
{
private:
    int * array = NULL;
    int count = 0;
    int size = 0;

public:
    Bag();
    Bag(int size);
    Bag(const Bag & src);
    ~Bag();
    void insert(const int & x);
    void remove(const int & x);
    bool exist(const int & x);
    const Bag & operator = (const Bag & src);
    void operator += (const int & x);

    friend ostream & operator << (ostream & os, const Bag & bag);
};