#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

int main()
{
    string fileName;
    int number;
    ifstream file;

    cout << "Zadej nazev souboru:" << endl;
    cin >> fileName;
    cout << "Zadejte cislo: " << endl;
    cin >> number;

    file.open(fileName);
    if(file.fail())
    {
        cout << "Soubor neexistuje!" << endl;
        return 0;
    }
    int num;
    file >> num;
    if(num == number)
    {
        cout << "Stejne cislo." << endl;
    }
    else
    {
        cout << "Jine cislo." << endl;
    }
    return 0;
}
