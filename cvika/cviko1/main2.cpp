#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

int main()
{
    string fileName;
    int count;
    vector<int> data;

    cout << "Zadej nazev souboru:" << endl;
    cin >> fileName;
    cout << "Zadejte pocet cisel: " << endl;
    cin >> count;
    cout << "Zadejte " << count << " cisel:" << endl;
    for (int i = 0; i < count; i++) {
        int val;
        cin >> val;
        data.push_back(val);
    }

    ofstream file(fileName);
    for(auto &i: data)
    {
        file << i << endl;
    }
    file.close();
    return 0;
}
