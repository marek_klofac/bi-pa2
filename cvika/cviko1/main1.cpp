#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
    int x;
    string c;
    cout << "Zadej cislo a slovo:" << endl;
    cin >> x >> setw(10) >> c;
    cout << "Zadal jsi: " << x << " " << c << endl;
    return 0;
}
