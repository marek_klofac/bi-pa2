#include <iostream>

class Complex
{
    private:
        int real;
        int imag;

    public:
        Complex operator + (const Complex & other);
        Complex operator - (const Complex & other);
        Complex operator * (const Complex & other);
        Complex operator / (const Complex & other);
        bool operator == (const Complex & other);
        double Abs() const;
        int GetReal() const;
        int GetImag() const;
        Complex(int real, int imag);
        ~Complex();
};
std::ostream & operator << (std::ostream & os, const Complex & other);
