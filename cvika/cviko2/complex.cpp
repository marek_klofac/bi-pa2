#include <iostream>
#include <math.h>
#include "Complex.hpp"

// Source: "http://candcplusplus.com/c-how-to-add-subtract-multiply-divide-two-complex-numbers"

Complex Complex::operator + (const Complex & other)
{
    return(Complex(this->real + other.real, this->imag + other.imag));
}
Complex Complex::operator - (const Complex & other)
{
    return(Complex(this->real + other.real, this->imag - other.imag));
}
Complex Complex::operator * (const Complex & other)
{
    //(x1x2−y1y2)+(x1y2+x2y1)i
    return(Complex(this->real * other.real - this->imag * other.imag,
                   this->real * other.imag + this->imag * other.real));
}
Complex Complex::operator / (const Complex & other)
{
    // (ac+bd)/(c2 + d2) + (bc-ad)/(c2 + d2)i
    return(Complex((this->real*other.real+this->imag*other.imag)/(pow(other.real,2)+pow(other.imag,2)),
                   (other.real*this->imag-this->real*other.imag)/(pow(other.real,2)+pow(other.imag,2))
               )
    );
}
bool Complex::operator == (const Complex & other)
{
    return(this->real == other.real && this->imag == other.imag);
}
double Complex::Abs() const
{
    return(sqrt(pow(this->real, 2) + pow(this->imag, 2)));
}
int Complex::GetReal() const
{
    return(this->real);
}
int Complex::GetImag() const
{
    return(this->imag);
}

Complex::Complex(int real, int imag)
{
    this->real = real;
    this->imag = imag;
}
Complex::~Complex(){}

std::ostream & operator << (std::ostream & os, const Complex & object)
{
    os << object.GetReal() << " + " << object.GetImag() << "i" << std::endl;
    return os;
}
