#include "Complex.hpp"
#include <iostream>

int main()
{
    Complex a(2, 2);
    Complex b(3, 3);
    Complex c(4, 4);

    std::cout << a << std::endl;

    return 0;
}
