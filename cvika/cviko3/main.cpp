#include <iostream>
#include <string>

template <typename T>
class Node
{
private:
    T data;
    Node<T> * next;
public:
    void setData(const T & newData)
    {
        data = newData;
        next = NULL;
    }
    T getData()
    {
        return data;
    }
    void setNext(Node<T> * newNext)
    {
        next = newNext;
    }
    Node<T> * getNext()
    {
        return next;
    }
    Node(){}
    ~Node(){}
};

template <typename T>
class Queue
{
private:
    Node<T> * head;
    int count;
    int countMax;

public:
    bool push(const T & data)
    {
        if(isEmpty())
        {
            head = new Node<T>;
            head->setData(data);
        }
        else
        {
            if(count < countMax)
            {
                Node<T> * tmp = new Node<T>;
                tmp->setData(data);
                tmp->setNext(head);
                head = tmp;
                count++;
            }
            else
            {
                std::cout << "Queue is full." << std::endl;
                return false;
            }
        }
        return true;
    }
    void pop()
    {
        Node<T> * tmp = head->getNext();
        delete head;
        head = tmp;
        count--;
    }
    Node<T> * front() const
    {
        return head;
    }
    bool isEmpty() const
    {
        return head == NULL;
    }
    void print() const
    {
        Node<T> * tmp = head;
        while(tmp != NULL)
        {
            std::cout << "==========" << std::endl;
            std::cout << "Queue:" << std::endl;
            std::cout << head->getData() << std::endl;
            tmp = tmp->getNext();
        }
    }
    Queue(int max): head(NULL), count(0), countMax(max){}
    ~Queue(){}
};

int main()
{
    Queue<std::string> queue(10);
    queue.push("aghoj");
    queue.push("ge");
    queue.push("s");
    queue.print();

    //queue.pop();
    //queue.print();

    //queue.front();

    return 0;
}
