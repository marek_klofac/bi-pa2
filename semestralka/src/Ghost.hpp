#pragma once
#include <string>
#include "Player.hpp"

class Ghost: public Character
{
private:
    std::string name;
    enum DEFAULT_STATS
    {
        HEALTH        = 50,
        ATTACK_DAMAGE = 5,
        ABILITY_POWER = 50,
        DEFENSE       = 10
    };

public:

    /**
     * @brief   Construct a new Ghost object using given coordinates
     * @param   _x      X coordinate 
     * @param   _y      Y coordinate
     * @param   _name   Name of the monster
     */
    Ghost(int _x, int _y, std::string _name);

    /**
     * @brief   Construct a new Ghost object using a save file (loading)
     * @param   _saveFile   Save file from which to load and initialize variables
     */
    Ghost(FileHandler & _saveFile);

    /**
	 * @brief	Ghost draws its symbol to window at current position
	 * @param 	_window 
	 */
	void Draw(WINDOW * _window) override;

    /**
     * @brief   Display current ghost's stats into nCurses window
     * @param   _window     Window pointer where to display
     */
    void DrawEncounter(WINDOW * _window) override;

    /**
     * @brief   This method is called, when something encounters Ghost.
     *          E.g. Player encouters this Ghost
     * @param   _other    Pointer to GameObject, that collided with Goblin
     * @return  true    GameObject should be destroyed after this interaction
     * @return  false   GameObject should not be destroyed after this interaction
     */
    bool Interact(const std::shared_ptr<Player> & _player) override;

    /**
     * @brief   Virtual method for saving the current state of Ghost.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    void Save(FileHandler & _saveFile) override;

    /**
     * @brief   Virtual method for loading previously saved state of Ghost.
     *          Each class that derives from this should also know how to 
     *          load it's own values from this text file
     * @param   _saveFile   Text file where to store data
     */
    void Load(FileHandler & _saveFile) override;

    /**
     * @brief   Calculating new position, where Ghost would move to and returning it.
     *          Collisions solved layers above in GameSystem. Taking into account
     *          AI for next step.
     * @return  Position        Newly calculated position, where the Ghost would move to
     */
    Position NextPosition(const Position & _playerPosition) override;

    /**
     * @brief   Returns the value of how much the attack
     *          is powerful. 
     * @return  int     Attack power all together 
     */
    int CalculateAttack() override;

    /**
     * @brief   Ghost takes damage appropriately,
     *          taking into account it's defenses
     * @param   _damage     Pure damage given by other Character
     */
    void TakeDamage(int _damage) override;
};