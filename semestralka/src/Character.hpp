#pragma once
#include <cmath>
#include "GameObject.hpp"
#include "Constants.hpp"

class Character: public GameObject
{
protected:
    int health;
    int attackDamage;
    int abilityPower;
    int defense;

public:
    /**
     * @brief   Construct a empty Character object
     */
    Character();
    
    /**
     * @brief   Construct a new Character object
     * 
     * @param   _x              X coordinate
     * @param   _y              Y coordinate
     * @param   _symbol         Char symbol
     * @param   _encounterText  Text shown when encoutering this character
     * @param   _interactText   Text shown when interacting this character
     * @param   _health         Health value
     * @param   _attackDamage   Attack damage value
     * @param   _abilityPower   Ability power value
     * @param   _defense        Defense value
     */
    Character(
        int _x,
        int _y,
        char _symbol,
        std::string _encounterText,
        std::string _interactText,
        int _health,
        int _attackDamage,
        int _abilityPower,
        int _defense
    );

    int GetHealth();

    int GetAttackDamage();

    int GetAbilityPower();

    int GetDefense();

    /**
     * @brief   Calculating and returning the next position the GameObject
     *          would move to. Later check for any collisions  
     * @param   _playerPosition             Some GameObjects may calculate it's movement dependent on Player's position
     * @return  Position                    Future position to where the GameObject would move
     */
    virtual Position NextPosition(const Position & _playerPosition) = 0;

    /**
     * @brief   Virtual method for saving the current state of GameObject.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    void Save(FileHandler & _saveFile) override;

    /**
     * @brief   Virtual method for loading previously saved state of GameObject.
     *          Each class that derives from this should also know how to 
     *          load it's own values from this text file
     * @param   _saveFile   Text file where to store data
     */
    void Load(FileHandler & _saveFile) override;

    /**
     * @brief   Returns the value of how much the attack
     *          is powerful. This method has to be overriden. E.g
     *          Walls will not give any damage
     * @return  int     Attack power all together 
     */
    virtual int CalculateAttack() = 0;

    /**
     * @brief   Character takes damage appropriately,
     *          taking into account it's defenses
     * @param   _damage     Pure damage given by other Character
     */
    virtual void TakeDamage(int _damage) = 0;
};