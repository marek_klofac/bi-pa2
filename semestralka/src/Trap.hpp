#pragma once
#include "GameObject.hpp"
#include "Player.hpp"

class Trap: public GameObject
{
private:
    int damage;

public:
    /**
     * @brief   Construct a new Trap object using given coordinates
     * @param   _x      X coordinate 
     * @param   _y      Y coordinate
     */
    Trap(int _x, int _y);

    /**
     * @brief   Construct a new Trap object using a save file (loading)
     * @param   _saveFile   Save file from which to load and initialize variables
     */
    Trap(FileHandler & _saveFile);

    /**
	 * @brief	Trap draws its symbol to window at current position
	 * @param 	_window 
	 */
	void Draw(WINDOW * _window) override;

    /**
     * @brief   This method is called, when something encounters Trap.
     *          E.g. Player encouters this Trap
     * @param   _other  Pointer to GameObject, that collided with Goblin
     * @return  true    GameObject should be destroyed after this interaction
     * @return  false   GameObject should not be destroyed after this interaction
     */
    bool Interact(const std::shared_ptr<Player> & _player) override;

    /**
     * @brief   Virtual method for saving the current state of Trap.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    virtual void Save(FileHandler & _saveFile) override;

    /**
     * @brief   Virtual method for laoding previously saved state of Trap.
     *          Each class that derives from this should also know how to 
     *          load it's own values into a text file
     * @param   _saveFile   Text file from where to load data
     */
    virtual void Load(FileHandler & _saveFile) override;
};