#include "Position.hpp"

Position::Position()
{

}

Position::Position(int _x, int _y): x(_x), y(_y)
{

}

int Position::GetX() const
{
    return x;
}

int Position::GetY() const
{
    return y;
}

void Position::SetX(int _x)
{
    x = _x;
}

void Position::SetY(int _y)
{
    y = _y;
}