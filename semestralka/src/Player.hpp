#pragma once
#include <ncurses.h>
#include <fstream>
#include "Character.hpp"
#include "Inventory.hpp"

class Player: public Character
{
private:
    int experience;
    int rotation;
    Inventory inventory;
    enum DEFAULT_STATS
    {
        HEALTH        = 100,
        ATTACK_DAMAGE = 10,
        ABILITY_POWER = 10,
        DEFENSE       = 10
    };

public:

    /**
     * @brief   Construct a new Player object using given coordinates
     * @param   _x  X coordinate 
     * @param   _y  Y coordinate
     */
    Player(int _x, int _y);

    /**
     * @brief   Construct a new Player object using a save file (loading)
     * @param   _saveFile   Save file from which to load and initialize variables
     */
    Player(FileHandler & _saveFile);

    /**
	 * @brief	Player draws his symbol to window at current position
	 * @param 	_window 
	 */
	void Draw(WINDOW * _window) override;

    /**
     * @brief   Display current player's stats into nCurses window
     * @param   _window     Window pointer where to display
     */
    void DrawEncounter(WINDOW * _window) override;

    /**
     * @brief   This method is called, when something encounters Player.
     *          E.g. Goblin encounters Player
     * @param   _other  Pointer to GameObject, that collided with Goblin
     * @return  true    GameObject should be destroyed after this interaction
     * @return  false   GameObject should not be destroyed after this interaction
     */
    bool Interact(const std::shared_ptr<Player> & _player) override;

    /**
     * @brief   Virtual method for saving the current state of Player.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    void Save(FileHandler & _saveFile) override;

    /**
     * @brief   Virtual method for loading previously saved state of Player.
     *          Each class that derives from this should also know how to 
     *          load it's own values from this text file
     * @param   _saveFile   Text file where to store data
     */
    void Load(FileHandler & _saveFile) override;

    /**
     * @brief   Calculating new position, where Player would move to and returning it.
     *          Collisions solved layers above in GameSystem. Taking into account
     *          Player's current rotation.
     * @return  _pressedKey     UP DOWN LEFT or RIGHT (ncurses constants)
     */
    Position NextPosition(const Position & _playerPosition) override;

    /**
     * @brief   Returns the value of how much the attack
     *          is powerful. 
     * @return  int     Attack power all together 
     */
    int CalculateAttack() override;

    /**
     * @brief   Player takes damage appropriately,
     *          taking into account it's defenses
     * @param   _damage     Pure damage given by other Character
     */
    void TakeDamage(int _damage) override;

    /**
     * @brief   Dependent on user's inp[ut in UI, boost
     *          appropriate abilites on character
     * @param	_strength	    Obtained value to be added to attackDamage
     * @param	_intelligence	Obtained value to be added to abilityPower
     * @param	_stamina	    Obtained value to be added to defense
     */
	void SetUIBuffs(int _strength, int _intelligence, int _stamina);

    /**
     * @brief   Get the Experience value
     * 
     * @return  int     Current Player's experience
     */
    int GetExperience();

    /**
     * @brief   Upon player killing a monster, player gains some experience points
     * @param   _xp     Obtained xp
     */
    void GainExperience(int _xp);

    /**
     * @brief   Upon having more than 100 experience points, player levels up
     *          All basic stats are increased by 5 and player is healed to full hp
     */
    void LevelUp();

    /**
     * @brief   Set the rotation of Player
     * @param   _rotation   New rotation to be set
     */
    void SetRotation(int _rotation);

    /**
     * @brief   Return a reference to inventory
     *          to be modified
     */
    Inventory & GetInventory();
};
