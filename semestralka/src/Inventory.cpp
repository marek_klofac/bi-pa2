#include "Inventory.hpp"
#include "Item.hpp"

int Inventory::Size() const
{
	return items.size();
}

void Inventory::AddItem(const std::shared_ptr<Item> & _item)
{
	if(items.size() == MAX_SIZE)
    {
        throw(InventoryFullException("Inventory full! Could not pick up item."));
    }
    items.push_back(_item);
}

void Inventory::Draw(WINDOW * _window)
{
    wattron(_window, A_UNDERLINE);
    mvwprintw(_window, 16, 104, "INVENTORY:");
    wattroff(_window, A_UNDERLINE);
    unsigned int i = 0;
    while(i < items.size())
    {
        mvwprintw(_window, 17+i, 100, items[i]->GetName().c_str());
        i++;
    }
    // Print all bonuses next to player stats
    wattron(_window, COLOR_PAIR(Constants::NCURSES_COLORS::BONUS));
    mvwprintw(_window, 31, 106, ("+" + std::to_string(GetBonusAttackDamage())).c_str());
    mvwprintw(_window, 32, 106, ("+" + std::to_string(GetBonusAbilityPower())).c_str());
    mvwprintw(_window, 33, 106, ("+" + std::to_string(GetBonusDefense())).c_str());
    wattroff(_window, COLOR_PAIR(Constants::NCURSES_COLORS::BONUS));

    // Print the rest as empty
    while(i < MAX_SIZE)
    {
        mvwprintw(_window, 17+i, 100, "<empty>");
        i++;
    }
}

int Inventory::GetBonusAttackDamage()
{
    int tmp = 0;
    for(auto & item: items)
    {
        tmp += item->GetBonusAttackDamage();
    }
    return tmp;
}

int Inventory::GetBonusAbilityPower()
{
    int tmp = 0;
    for(auto & item: items)
    {
        tmp += item->GetBonusAbilityPower();
    }
    return tmp;
}

int Inventory::GetBonusDefense()
{
    int tmp = 0;
    for(auto & item: items)
    {
        tmp += item->GetBonusDefense();
    }
    return tmp;
}

void Inventory::Save(FileHandler & _saveFile)
{
    _saveFile.WriteNumber(items.size());
	for(auto & it: items)
    {
        it->SaveFromInventory(_saveFile);
    }
}

void Inventory::Load(FileHandler & _saveFile)
{
	unsigned int itemCount = _saveFile.ReadNumber();
    for(unsigned int i = 0; i < itemCount; i++)
    {
        items.push_back(std::make_shared<Item>(_saveFile, false));
    }
    if(items.size() != itemCount)
    {
        throw(SaveFileException("Save file corrupted or modified. Invetory loading failed. Too many or too few items."));
    }
}