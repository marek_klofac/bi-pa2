#include "Ghost.hpp"

Ghost::Ghost(int _x, int _y, std::string _name):
    Character(
        _x,
        _y,
        static_cast<char>(Constants::SYMBOLS::GHOST),
        "You have encoutered a GHOST called: " + _name,
        "default ghost interact text",
        DEFAULT_STATS::HEALTH,
        DEFAULT_STATS::ATTACK_DAMAGE,
        DEFAULT_STATS::ABILITY_POWER,
        DEFAULT_STATS::DEFENSE
    ),
    name(_name)
{}

Ghost::Ghost(FileHandler & _saveFile)
{
    Load(_saveFile);
}

void Ghost::Draw(WINDOW * _window)
{
    mvwaddch(_window, position.GetY(), position.GetX(), symbol | COLOR_PAIR(Constants::NCURSES_COLORS::GHOST));
    refresh();
}

void Ghost::DrawEncounter(WINDOW * _window)
{
    mvwprintw(_window, 29, 55, "|");
    wattron(_window, A_UNDERLINE);
    mvwprintw(_window, 29, 57, "GHOST:");
    wattroff(_window, A_UNDERLINE);
    mvwprintw(_window, 30, 55, "| Name: %s", name.c_str());
    mvwprintw(_window, 31, 55, "| Health:        %d", health);
    mvwprintw(_window, 32, 55, "| Attack damage: %d", attackDamage);
    mvwprintw(_window, 33, 55, "| Ability power: %d", abilityPower);
    mvwprintw(_window, 34, 55, "| Defense:       %d", defense);
}

bool Ghost::Interact(const std::shared_ptr<Player> & _player)
{
    // Deal damage to Player that encoutered this
    _player->TakeDamage(CalculateAttack());

    // Take damage from Player that encoutered this
    this->TakeDamage(_player->CalculateAttack());

    if(health <= 0)
    {
        int xp = (std::rand() % 7) + 1;
        _player->GainExperience(xp);
        interactText = "Ghost killed, gained " + std::to_string(xp) + " XP";
        return true;
    }
    else
    {
        interactText = "SLASH! Fight in progress, check health values.";
        return false;
    }
}

void Ghost::Save(FileHandler & _saveFile)
{
    Character::Save(_saveFile);
    _saveFile.WriteString(name);
}

void Ghost::Load(FileHandler & _saveFile)
{
    symbol = static_cast<char>(Constants::SYMBOLS::GHOST);
    Character::Load(_saveFile);
    name = _saveFile.ReadString();
}

Position Ghost::NextPosition(const Position & _playerPosition)
{
    switch(std::rand() % 4)
    {
        case 0:
            return Position(position.GetX(), position.GetY() - 1);
        case 1:
            return Position(position.GetX(), position.GetY() + 1);
        case 2:
            return Position(position.GetX() - 1, position.GetY());
        case 3:
            return Position(position.GetX() + 1, position.GetY());
        default:
            return position;
    }
}

int Ghost::CalculateAttack()
{
    return attackDamage;
}

void Ghost::TakeDamage(int _damage)
{
    health -= _damage * (1 - (defense / 100.0));
}