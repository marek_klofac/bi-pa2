#pragma once
#include <ncurses.h>
#include <vector>
#include <memory>
#include <fstream>
#include "BaseException.hpp"
#include "FileHandler.hpp"

class Item;

class Inventory
{
private:
	const unsigned int MAX_SIZE = 5;
	std::vector<std::shared_ptr<Item>> items;

public:

	/**
	 * @brief   Returns the current number of items in inventory
	 * @return  int     Number of items
	 */
	int Size() const;

	/**
	 * @brief   This method adds an Item to the Inventory.
	 * @param	item	Item to be added to inventory
     * @return  true    Added to inventory
     * @return  false   Inventory full, item not added
	 */
	void AddItem(const std::shared_ptr<Item> & _item);

    /**
     * @brief   Draw the current items of inventory into a
     *          ncurses window
     * @param   _window     Window to draw to
     */
    void Draw(WINDOW * _window);

    /**
     * @brief   Get the bonus attack damage of this item
     * @return  int     Bonus
     */
    int GetBonusAttackDamage();
    
    /**
     * @brief   Get the bonus ability power of this item
     * @return  int     Bonus
     */
    int GetBonusAbilityPower();

    /**
     * @brief   Get the bonus defense of this item
     * @return  int     Bonus
     */
    int GetBonusDefense();

	/**
	 * @brief   Saves the current state of inventory in to a save file
	 * @param   _saveFile   Text file to save to
	 */
	void Save(FileHandler & _saveFile);

	/**
	 * @brief   Load the saved state of inventory from a save file
	 * @param   _saveFile   Text file to load from
	 */
	void Load(FileHandler & _saveFile);
};