#include "Wall.hpp"

Wall::Wall(int _x, int _y):
    GameObject(
        _x,
        _y,
        static_cast<char>(Constants::SYMBOLS::WALL),
        "You have encoutered a WALL.",
        "Nothing to do here."
    )
{}

Wall::Wall(FileHandler & _saveFile)
{
    Load(_saveFile);
}

void Wall::Draw(WINDOW * _window)
{
    mvwaddch(_window, position.GetY(), position.GetX(), symbol | COLOR_PAIR(Constants::NCURSES_COLORS::WALL));
    refresh();
}

bool Wall::Interact(const std::shared_ptr<Player> & _player)
{
    return false;
}

void Wall::Save(FileHandler & _saveFile)
{
    GameObject::Save(_saveFile);
}

void Wall::Load(FileHandler & _saveFile)
{
    symbol = static_cast<char>(Constants::SYMBOLS::WALL);
    GameObject::Load(_saveFile);
}