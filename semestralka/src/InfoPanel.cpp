#include "InfoPanel.hpp"

void InfoPanel::Clear(WINDOW * _window) const
{
	mvwprintw (_window, 30, 1, "                                                              ");
	mvwprintw (_window, 30, 78, "    ");
	mvwprintw (_window, 31, 1, "                                                              ");
	mvwprintw (_window, 31, 78, "  ");
	mvwprintw (_window, 32, 1, "                                                              ");
	mvwprintw (_window, 32, 78, "    ");
	mvwprintw (_window, 33, 1, "                                                              ");
	mvwprintw (_window, 33, 78, "  ");
	mvwprintw (_window, 34, 1, "                                                              ");
	mvwprintw (_window, 34, 78, "  ");
}

void InfoPanel::Draw(WINDOW * _window)
{	
    Clear(_window);
    
    //TEXT
	mvwprintw (_window, 29, 1, "|");
	wattron (_window, A_UNDERLINE);
	mvwprintw (_window, 29, 3, "INFO PANEL:");
	wattroff (_window, A_UNDERLINE);
	mvwprintw (_window, 30, 1, "|");
	mvwprintw (_window, 30, 3, title.c_str ());
	mvwprintw (_window, 31, 1, "|");
    // Text can be multiple lines
    std::istringstream s(text);
	std::string line;
    int i = 0;
    while(std::getline(s, line))
    {
        mvwprintw (_window, 31+i, 3, line.c_str ());
        i++;
    }
	mvwprintw (_window, 32, 1, "|");
	mvwprintw (_window, 33, 1, "|");
	mvwprintw (_window, 34, 1, "|");

    // PLAYER
    player->DrawEncounter(_window);

    // ENCOUTERED GAME OBJECT
    if(encountered != nullptr)
    {
        encountered->DrawEncounter(_window);
    }
    // LEGEND
    wattron   (_window, A_UNDERLINE);
	mvwprintw (_window, 2,  105, "LEGEND");
	wattroff  (_window, A_UNDERLINE);
	mvwaddch  (_window, 4,  103, static_cast<char>(Constants::SYMBOLS::PLAYER) | COLOR_PAIR(Constants::NCURSES_COLORS::PLAYER));
    mvwprintw (_window, 4,  105, "... Player");
	mvwaddch  (_window, 5,  103, static_cast<char>(Constants::SYMBOLS::WALL) | COLOR_PAIR(Constants::NCURSES_COLORS::WALL));
    mvwprintw (_window, 5,  105, "... Wall");
    mvwaddch  (_window, 6,  103, static_cast<char>(Constants::SYMBOLS::ITEM) | COLOR_PAIR(Constants::NCURSES_COLORS::ITEM));
	mvwprintw (_window, 6,  105, "... Item");
    mvwaddch  (_window, 7,  103, static_cast<char>(Constants::SYMBOLS::TRAP) | COLOR_PAIR(Constants::NCURSES_COLORS::TRAP));
	mvwprintw (_window, 7,  105, "... Trap");
    mvwaddch  (_window, 8,  103, static_cast<char>(Constants::SYMBOLS::GOBLIN) | COLOR_PAIR(Constants::NCURSES_COLORS::GOBLIN));
	mvwprintw (_window, 8,  105, "... Goblin");
    mvwaddch  (_window, 9,  103, static_cast<char>(Constants::SYMBOLS::DRAGON) | COLOR_PAIR(Constants::NCURSES_COLORS::DRAGON));
	mvwprintw (_window, 9, 105, "... Dragon");
    mvwaddch  (_window, 10,  103, static_cast<char>(Constants::SYMBOLS::GHOST) | COLOR_PAIR(Constants::NCURSES_COLORS::GHOST));
	mvwprintw (_window, 10, 105, "... Ghost");
	wmove (_window, 34, 119);
}

void InfoPanel::Update(const std::string & _title, const std::string & _text, const std::shared_ptr<GameObject> & _encountered)
{
	title = _title;
    text = _text;
    encountered = _encountered;
}

void InfoPanel::SetPlayer(const std::shared_ptr<Player> & _player)
{
    player = _player;
}