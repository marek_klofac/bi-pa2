#include "FileHandler.hpp"

void FileHandler::OpenForWrite(const std::string & _fileName)
{
    file.open(_fileName, std::fstream::out);
	if(!file.good())
	{
		throw(FileHandlerException("Save file either cannot be created or couldn't be opened."));
	}
}

void FileHandler::OpenForRead(const std::string & _fileName)
{
    file.open(_fileName, std::fstream::in);
	if(!file.good())
	{
		throw(FileHandlerException("Save file either doesn't exist or couldn't be opened."));
	}
}

void FileHandler::Close()
{
    file.close();
}

int FileHandler::ReadNumber()
{
    std::string line;
    std::getline(file, line);
    if(!std::all_of(line.begin(), line.end(), ::isdigit) || line.empty() || !file.good())
    {
        throw(SaveFileException("Save file corrupted or modified. Expected a number."));
    }
    return std::stoi(line);
}

char FileHandler::ReadChar()
{
    std::string line;
    std::getline(file, line);
    if(line.size() != 1 || !file.good())
    {
        throw(SaveFileException("Save file corrupted or modified. Expected a char."));
    }
    return line[0];
}

std::string FileHandler::ReadString()
{
    std::string line;
    std::getline(file, line);
    if(line.empty() || !file.good())
    {
        throw(SaveFileException("Save file corrupted or modified. Expected a string."));
    }
    return line;
}

void FileHandler::ReadEnd()
{
    std::string line;
    std::getline(file, line);
    if(line != END_SEQUENCE || !file.eof())
    {
        throw(SaveFileException("Save file corrupted or modified. Unexpected file ending."));
    }
}

void FileHandler::WriteNumber(int _number)
{
    file << _number << std::endl;
}

void FileHandler::WriteChar(char _char)
{
    file << _char << std::endl;
}

void FileHandler::WriteString(std::string _string)
{
    file << _string << std::endl;
}

void FileHandler::WriteEnd()
{
    file << "***END***";
}