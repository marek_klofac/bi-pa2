#include "BaseException.hpp"

BaseException::BaseException(int _code, std::string _message): code(_code), message(_message)
{

}

std::string BaseException::GetMessage() const
{
    return message;
}

std::ostream & operator << (std::ostream & _os, const BaseException & _e)
{
    _os << "ERROR: " << _e.code << " - " << _e.message << std::endl;
    return _os;
}

NoColorTermException::NoColorTermException(std::string _message): BaseException(1, _message){}

MapFileException::MapFileException(std::string _message): BaseException(2, _message){}

SaveFileException::SaveFileException(std::string _message): BaseException(3, _message){}

NonMovableObjectException::NonMovableObjectException(std::string _message): BaseException(4, _message){}

MapBoundariesException::MapBoundariesException(std::string _message): BaseException(5, _message){}

InventoryFullException::InventoryFullException(std::string _message): BaseException(6, _message){}

FileHandlerException::FileHandlerException(std::string _message): BaseException(7, _message){}

MoveOutOfBoundsException::MoveOutOfBoundsException(std::string _message): BaseException(8, _message){}

InternalErrorException::InternalErrorException(std::string _message): BaseException(404, _message){}