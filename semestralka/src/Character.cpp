#include "Character.hpp"

Character::Character()
{

}

Character::Character(
    int _x,
    int _y,
    char _symbol,
    std::string _encounterText,
    std::string _interactText,
    int _health,
    int _attackDamage,
    int _abilityPower,
    int _defense
):
    GameObject(_x, _y, _symbol, _encounterText, _interactText),
    health(_health),
    attackDamage(_attackDamage),
    abilityPower(_abilityPower),
    defense(_defense)
{}

int Character::GetHealth()
{
    return health;
}

int Character::GetAttackDamage()
{
    return attackDamage;
}

int Character::GetAbilityPower()
{
    return abilityPower;
}

int Character::GetDefense()
{
    return defense;
}

void Character::Save(FileHandler & _saveFile)
{
    GameObject::Save(_saveFile);
    _saveFile.WriteNumber(health);
    _saveFile.WriteNumber(attackDamage);
    _saveFile.WriteNumber(abilityPower);
    _saveFile.WriteNumber(defense);
}

void Character::Load(FileHandler & _saveFile)
{
    GameObject::Load(_saveFile);
    health = _saveFile.ReadNumber();
    attackDamage = _saveFile.ReadNumber();
    abilityPower = _saveFile.ReadNumber();
    defense = _saveFile.ReadNumber();
}