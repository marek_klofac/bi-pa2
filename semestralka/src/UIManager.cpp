#include "UIManager.hpp"

UIManager::UIManager()
{
    initscr();
	// Check if current terminal has colors
	if(has_colors() == FALSE)
	{
		endwin();
		throw(NoColorTermException("Ncurses couldn't be started - colors not allowed in current terminal."));
	}
	window = newwin(WINDOW_HEIGHT, WINDOW_WIDTH, 0, 0);

	// Match to contants their FOREGROUND and BACKGROUND
	start_color();
	init_pair(Constants::NCURSES_COLORS::PLAYER, COLOR_BLACK,   COLOR_GREEN);
    init_pair(Constants::NCURSES_COLORS::WALL,   COLOR_BLACK,   COLOR_WHITE);
    init_pair(Constants::NCURSES_COLORS::ITEM,   COLOR_GREEN,   COLOR_BLACK);
    init_pair(Constants::NCURSES_COLORS::TRAP,   COLOR_MAGENTA, COLOR_BLACK);
    init_pair(Constants::NCURSES_COLORS::GOBLIN, COLOR_RED,     COLOR_BLACK);
    init_pair(Constants::NCURSES_COLORS::DRAGON, COLOR_RED,     COLOR_BLACK);
    init_pair(Constants::NCURSES_COLORS::GHOST,  COLOR_RED,     COLOR_BLACK);
    init_pair(Constants::NCURSES_COLORS::BONUS,  COLOR_GREEN,   COLOR_BLACK);
	// ... other pairs, add to enum in Constants.hpp ...

    DrawWelcomeScreen();
    getch ();
}
UIManager::~UIManager()
{
	CloseWindow();
}

WINDOW * UIManager::GetWindow ()
{
	return window;
}

InfoPanel & UIManager::GetInfoPanel()
{
    return infoPanel;
}

void UIManager::DrawWelcomeScreen() const
{
	ClearWindow();
	mvwprintw (window, 3, 18, "   ____        _                ____                                           ");
	mvwprintw (window, 4, 18, "  / ___| _ __ (_) _ __ ___     |  _ \\  _   _  _ __    __ _   ___   ___   _ __  ");
	mvwprintw (window, 5, 18, " | |  _ | '__|| || '_ ` _ \\    | | | || | | || '_ \\  / _` | / _ \\ / _ \\ | '_ \\ ");
	mvwprintw (window, 6, 18, " | |_| || |   | || | | | | |   | |_| || |_| || | | || (_| ||  __/| (_) || | | |");
	mvwprintw (window, 7, 18, "  \\____||_|   |_||_| |_| |_|   |____/  \\__,_||_| |_| \\__, | \\___| \\___/ |_| |_|");
	mvwprintw (window, 8, 18, "                                                     |___/                     ");
	mvwprintw (window, 23, 40, "---- Press any key to PLAY ----");
	mvwprintw (window, 31, 1, "RPG game created by Marek Klofac.");
	mvwprintw (window, 32, 1, "Created as a semestral project for PA2.");
	mvwprintw (window, 33, 1, "© All rights reserved 2020.");
	wmove (window, 34, 119);
	wrefresh (window);
}

void UIManager::DrawMainMenu() const
{
	ClearWindow();
	mvwprintw (window, 3, 32, "___  ___        _                                        ");
	mvwprintw (window, 4, 32, "|  \\/  |       (_)                                       ");
	mvwprintw (window, 5, 32, "| .  . |  __ _  _  _ __    _ __ ___    ___  _ __   _   _ ");
	mvwprintw (window, 6, 32, "| |\\/| | / _` || || '_ \\  | '_ ` _ \\  / _ \\| '_ \\ | | | |");
	mvwprintw (window, 7, 32, "| |  | || (_| || || | | | | | | | | ||  __/| | | || |_| |");
	mvwprintw (window, 8, 32, "\\_|  |_/ \\__,_||_||_| |_| |_| |_| |_| \\___||_| |_| \\__,_|");
}

void UIManager::DrawInGameMenu() const
{
	ClearWindow();
	mvwprintw (window, 3, 32, " _____                                                       _ ");
	mvwprintw (window, 4, 32, "|  __ \\                                                     | |");
	mvwprintw (window, 5, 32, "| |  \\/ __ _ _ __ ___   ___   _ __   __ _ _   _ ___  ___  __| |");
	mvwprintw (window, 6, 32, "| | __ / _` | '_ ` _ \\ / _ \\ | '_ \\ / _` | | | / __|/ _ \\/ _` |");
	mvwprintw (window, 7, 32, "| |_\\ \\ (_| | | | | | |  __/ | |_) | (_| | |_| \\__ \\  __/ (_| |");
	mvwprintw (window, 8, 32, " \\____/\\__,_|_| |_| |_|\\___| | .__/ \\__,_|\\__,_|___/\\___|\\__,_|");
    mvwprintw (window, 9, 32, "                             | |                               ");
    mvwprintw (window, 10, 32, "                             |_|                               ");
}

UIManager::MenuChoices UIManager::GetMainMenuChoice() const
{
    noecho();
	keypad(window, true);
    int choice = 0;
	int pressedKey = 0;

	while (true)
	{
		for (int i = 0; i < 4; ++i)
		{
			if (i == choice)
				wattron (window, A_REVERSE);
			mvwprintw (window, i+14, 52, MenuChoicesForDraw[i].c_str ());
			wattroff (window, A_REVERSE);
		}
		wmove (window, 34, 119);
		pressedKey = wgetch (window);

		switch (pressedKey)
		{
			case KEY_UP:
				choice--;
				if (choice == -1)
					choice = 0;
				break;
			case KEY_DOWN:
				choice++;
				if (choice == 4)
					choice = 3;
				break;
			default:
				break;
		}
        // Enter pressed, return selected option
		if (pressedKey == 10)
		{
			return static_cast<MenuChoices>(choice);
		}
	}
}

UIManager::MenuChoices UIManager::GetInGameMenuChoice() const
{
    noecho();
	keypad(window, true);
    int choice = 0;
	int pressedKey = 0;

	while (true)
	{
		for (int i = 0; i < 2; ++i)
		{
			if (i == choice)
				wattron (window, A_REVERSE);
			mvwprintw (window, i+14, 52, InGameMenuChoicesForDraw[i].c_str ());
			wattroff (window, A_REVERSE);
		}
		wmove (window, 34, 119);
		pressedKey = wgetch (window);

		switch (pressedKey)
		{
			case KEY_UP:
				choice--;
				if (choice == -1)
					choice = 0;
				break;
			case KEY_DOWN:
				choice++;
				if (choice == 2)
					choice = 1;
				break;
			default:
				break;
		}
        // Enter pressed, return selected option
		if (pressedKey == 10)
		{
			if(choice == 0)
            {
                return MenuChoices::SaveGame;
            }
            else
            {
                return MenuChoices::Quit;
            }
		}
	}
}

void UIManager::DrawOptions() const
{
	ClearWindow();
	mvwprintw (window, 3, 24, "______              _                             _                 _      ");
	mvwprintw (window, 4, 24, "| ___ \\            (_)                           | |               | |     ");
	mvwprintw (window, 5, 24, "| |_/ /  __ _  ___  _   ___    ___   ___   _ __  | |_  _ __   ___  | | ___ ");
	mvwprintw (window, 6, 24, "| ___ \\ / _` |/ __|| | / __|  / __| / _ \\ | '_ \\ | __|| '__| / _ \\ | |/ __|");
	mvwprintw (window, 7, 24, "| |_/ /| (_| |\\__ \\| || (__  | (__ | (_) || | | || |_ | |   | (_) || |\\__ \\");
	mvwprintw (window, 8, 24, "\\____/  \\__,_||___/|_| \\___|  \\___| \\___/ |_| |_| \\__||_|    \\___/ |_||___/");
	mvwprintw (window, 16, 32, "Arrows  ... Use arrow keys to move around.");
	mvwprintw (window, 17, 32, "E       ... Use E key to interact with objects.");
	mvwprintw (window, 18, 32, "Escape  ... Use ESC key to bring up the in game menu (save and quit).");
	wmove (window, 34, 119);
	wrefresh (window);
	while (true)
	{
		int pressedKey = wgetch (window);
		if (pressedKey == 27)
			break;
	}
}

void UIManager::DrawInfoPanel()
{
    infoPanel.Draw(window);
}

void UIManager::DrawCreatePlayer(const std::shared_ptr<Player> & _player)
{
    int skillPoints = 10;
	int strength = 0;
	int intelligence = 0;
	int stamina = 0;
	int pressedKey = 0;
	int row = 0;
	int column = 0;
	while (true)
	{
		ClearWindow();
		mvwprintw (window, 3, 8, " _____                _                                       _                          _            ");
		mvwprintw (window, 4, 8, "/  __ \\              | |                                     | |                        | |           ");
		mvwprintw (window, 5, 8, "| /  \\/_ __ ___  __ _| |_ ___   _   _  ___  _   _ _ __    ___| |__   __ _ _ __ __ _  ___| |_ ___ _ __ ");
		mvwprintw (window, 6, 8, "| |   | '__/ _ \\/ _` | __/ _ \\ | | | |/ _ \\| | | | '__|  / __| '_ \\ / _` | '__/ _` |/ __| __/ _ \\ '__|");
		mvwprintw (window, 7, 8, "| \\__/\\ | |  __/ (_| | ||  __/ | |_| | (_) | |_| | |    | (__| | | | (_| | | | (_| | (__| ||  __/ |   ");
		mvwprintw (window, 8, 8, " \\____/_|  \\___|\\__,_|\\__\\___|  \\__, |\\___/ \\__,_|_|     \\___|_| |_|\\__,_|_|  \\__,_|\\___|\\__\\___|_|   ");
		mvwprintw (window, 9, 8, "                                 __/ |                                                                ");
		mvwprintw (window, 10, 8, "                                |___/                                                                 ");
		mvwprintw (window, 13, 1, "Points remaining:");
		mvwprintw (window, 15, 25, "Strength - influences your combat against Dragons and Goblins.");
		mvwprintw (window, 16, 25, "Intelligence - influences your combat against Ghosts.");
		mvwprintw (window, 17, 25, "Stamina - influences your life total.");

		std::string skillPointsPrint = std::to_string (skillPoints);
		std::string strengthPrint = std::to_string (strength);
		std::string intelligencePrint = std::to_string (intelligence);
		std::string staminaPrint = std::to_string (stamina);
		mvwprintw (window, 13, 20, skillPointsPrint.c_str ());
		mvwprintw (window, 15, 20, strengthPrint.c_str ());
		mvwprintw (window, 16, 20, intelligencePrint.c_str ());
		mvwprintw (window, 17, 20, staminaPrint.c_str ());

		if (row == 0 && column == 0)
			wattron (window, A_REVERSE);
		mvwprintw (window, 15, 2, "-");
		wattroff (window, A_REVERSE);
		
		if (row == 0 && column == 1)
			wattron (window, A_REVERSE);
		mvwprintw (window, 15, 4, "+");
		wattroff (window, A_REVERSE);
		
		if (row == 1 && column == 0)
			wattron (window, A_REVERSE);
		mvwprintw (window, 16, 2, "-");
		wattroff (window, A_REVERSE);
		
		if (row == 1 && column == 1)
			wattron (window, A_REVERSE);
		mvwprintw (window, 16, 4, "+");
		wattroff (window, A_REVERSE);
		
		if (row == 2 && column == 0)
			wattron (window, A_REVERSE);
		mvwprintw (window, 17, 2, "-");
		wattroff (window, A_REVERSE);
		
		if (row == 2 && column == 1)
			wattron (window, A_REVERSE);
		mvwprintw (window, 17, 4, "+");
		wattroff (window, A_REVERSE);
		
		if (row == 3)
			wattron (window, A_REVERSE);
		mvwprintw (window, 20, 12, "PLAY");
		wattroff (window, A_REVERSE);

		wmove (window, 34, 119);
		pressedKey = wgetch (window);

		switch (pressedKey)
		{
			case KEY_UP:
				row--;
				if (row == -1)
					row = 0;
				break;
			case KEY_DOWN:
				row++;
				if (row == 4)
					row = 3;
				break;
			case KEY_LEFT:
				column--;
				if (column == -1)
					column = 0;
				break;
			case KEY_RIGHT:
				column++;
				if (column == 2)
					column = 1;
				break;
			default:
				break;
		}
        // 'Enter' pressed
		if (pressedKey == 10)
		{
			if (row == 0 && column == 0)
			{
				if (strength > 0)
				{
					strength--;
					skillPoints++;
				}
			}
			if (row == 0 && column == 1)
			{
				if (skillPoints > 0)
				{
					strength++;
					skillPoints--;
				}
			}
			if (row == 1 && column == 0)
			{
				if (intelligence > 0)
				{
					intelligence--;
					skillPoints++;
				}
			}
			if (row == 1 && column == 1)
			{
				if (skillPoints > 0)
				{
					intelligence++;
					skillPoints--;
				}
			}
			if (row == 2 && column == 0)
			{
				if (stamina > 0)
				{
					stamina--;
					skillPoints++;
				}
			}
			if (row == 2 && column == 1)
			{
				if (skillPoints > 0)
				{
					stamina++;
					skillPoints--;
				}
			}
            // User confirmed selection
			if (row == 3)
			{
                // Set abilities appropriately
                _player->SetUIBuffs(strength, intelligence, stamina);

                // Initialize InfoPanel's pointer to point to this player instance
                infoPanel.SetPlayer(_player);
				break;
			}
		}
	}
}

void UIManager::DrawEndScreen() const
{
	ClearWindow();
	mvwprintw (window, 3, 35, "__   __                    _  _            _");
	mvwprintw (window, 4, 35, "\\ \\ / /                   | |(_)          | |");
	mvwprintw (window, 5, 35, " \\ V /   ___   _   _    __| | _   ___   __| |");
	mvwprintw (window, 6, 35, "  \\ /   / _ \\ | | | |  / _` || | / _ \\ / _` |");
	mvwprintw (window, 7, 35, "  | |  | (_) || |_| | | (_| || ||  __/| (_| | _  _  _ ");
	mvwprintw (window, 8, 35, "  \\_/   \\___/  \\__,_|  \\__,_||_| \\___| \\__,_|(_)(_)(_)");
	mvwprintw (window, 10, 40, "Better luck next time.");
	wmove (window, 34, 119);
	wrefresh (window);
	getch ();
}

void UIManager::DrawWinScreen() const
{
    ClearWindow();
	mvwprintw (window, 3, 35, " _____                        _    _             _ ");
	mvwprintw (window, 4, 35, "|  __ \\                      | |  | |           | |");
	mvwprintw (window, 5, 35, "| |  \\/ __ _ _ __ ___   ___  | |  | | ___  _ __ | |");
	mvwprintw (window, 6, 35, "| | __ / _` | '_ ` _ \\ / _ \\ | |/\\| |/ _ \\| '_ \\| |");
	mvwprintw (window, 7, 35, "| |_\\ \\ (_| | | | | | |  __/ \\  /\\  / (_) | | | |_|");
	mvwprintw (window, 8, 35, " \\____/\\__,_|_| |_| |_|\\___|  \\/  \\/ \\___/|_| |_(_)");
	mvwprintw (window, 10, 40, "Thank you for playing!");
	wmove (window, 34, 119);
	wrefresh (window);
	getch ();
}

void UIManager::CloseWindow()
{
	delwin(window);
    endwin();
}

void UIManager::ClearWindow() const
{
    wclear(window);
    box (window, 0, 0);
    refresh();
}