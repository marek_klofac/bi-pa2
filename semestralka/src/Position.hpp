#pragma once

class Position
{
private:
    int x;
    int y;

public:
    /**
     * @brief Construct an empty Position object
     */
    Position();

    /**
     * @brief Construct a new Position object
     * @param   _x  Given x coordinate
     * @param   _y  Given y coordinate 
     */
    Position(int _x, int _y);

    /**
     * @brief Returns current x coordinate
     * 
     * @return  int     Returned x coordinate
     */
    int GetX() const;

    /**
     * @brief Returns current y coordinate
     * 
     * @return  int     Returned y coordinate
     */
    int GetY() const;

    /**
     * @brief   Set the x coordinate
     * @param   _x  New value
     */
    void SetX(int _x);

    /**
     * @brief   Set the y coordinate
     * @param   _y  New value
     */
    void SetY(int _y);

    bool operator ==(const Position & other)
    {
        return (this->x == other.x) && (this->y == other.y);
    };
};