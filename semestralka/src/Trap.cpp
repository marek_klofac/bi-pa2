#include "Trap.hpp"

Trap::Trap(int _x, int _y):
    GameObject(
        _x,
        _y,
        static_cast<char>(Constants::SYMBOLS::TRAP),
        "You have encoutered a TRAP.",
        "default trap interact text"
    ),
    damage(std::rand() % 20)
{}

Trap::Trap(FileHandler & _saveFile)
{
    Load(_saveFile);
}

void Trap::Draw(WINDOW * _window)
{
    mvwaddch(_window, position.GetY(), position.GetX(), symbol | COLOR_PAIR(Constants::NCURSES_COLORS::TRAP));
    refresh();
}

bool Trap::Interact(const std::shared_ptr<Player> & _player)
{
    _player->TakeDamage(damage);
    interactText = "Damage taken: " + std::to_string(damage);
    return true;
}

void Trap::Save(FileHandler & _saveFile)
{
    GameObject::Save(_saveFile);
    _saveFile.WriteNumber(damage);
}

void Trap::Load(FileHandler & _saveFile)
{
    symbol = static_cast<char>(Constants::SYMBOLS::TRAP);
    GameObject::Load(_saveFile);
    damage = _saveFile.ReadNumber();
}