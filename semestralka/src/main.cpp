#include <iostream>
#include <string>
#include "GameSystem.hpp"
#include "BaseException.hpp"

/**
 * @brief   This is the main file from where the game starts. More info about GameSystem can be found in its header file.
 */
int main()
{
	try
	{
		GameSystem engine;
		engine.Start();	
	}
	catch(const BaseException & e)
	{
		std::cout << e << std::endl;
		return 1;
	}
	return 0;
}
