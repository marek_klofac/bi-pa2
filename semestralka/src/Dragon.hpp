#pragma once
#include <string>
#include "Player.hpp"

class Dragon: public Character
{
private:
    std::string name;
    enum DEFAULT_STATS
    {
        HEALTH        = 75,
        ATTACK_DAMAGE = 2,
        ABILITY_POWER = 0,
        DEFENSE       = 20
    };

public:

    /**
     * @brief   Construct a new Dragon object using given coordinates
     * @param   _x      X coordinate 
     * @param   _y      Y coordinate
     * @param   _name   Name of the monster
     */
    Dragon(int _x, int _y, std::string _name);

    /**
     * @brief   Construct a new Dragon object using a save file (loading)
     * @param   _saveFile   Save file from which to load and initialize variables
     */
    Dragon(FileHandler & _saveFile);

    /**
	 * @brief	Dragon draws its symbol to window at current position
	 * @param 	_window 
	 */
	void Draw(WINDOW * _window) override;

    /**
     * @brief   Display current dragon's stats into nCurses window
     * @param   _window     Window pointer where to display
     */
    void DrawEncounter(WINDOW * _window) override;

    /**
     * @brief   This method is called, when something encounters Dragon.
     *          E.g. Player encouters this Dragon
     * @param   _other    Pointer to GameObject, that collided with Goblin
     * @return  true    GameObject should be destroyed after this interaction
     * @return  false   GameObject should not be destroyed after this interaction
     */
    bool Interact(const std::shared_ptr<Player> & _player) override;

    /**
     * @brief   Virtual method for saving the current state of Dragon.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    void Save(FileHandler & _saveFile) override;

     /**
     * @brief   Virtual method for loading previously saved state of Dragon.
     *          Each class that derives from this should also know how to 
     *          load it's own values from this text file
     * @param   _saveFile   Text file where to store data
     */
    void Load(FileHandler & _saveFile) override;

    /**
     * @brief   Calculating new position, where Dragon would move to and returning it.
     *          Collisions solved layers above in GameSystem. Taking into account
     *          AI for next step.
     * @return  Position        Newly calculated position, where the Dragon would move to
     */
    Position NextPosition(const Position & _playerPosition) override;

    /**
     * @brief   Returns the value of how much the attack
     *          is powerful. The less health the dragon has
     *          the higher the value of the attack
     * @return  int     Attack power all together 
     */
    int CalculateAttack() override;

    /**
     * @brief   Dragon takes damage appropriately,
     *          taking into account it's defenses
     * @param   _damage     Pure damage given by other Character
     */
    void TakeDamage(int _damage) override;
};