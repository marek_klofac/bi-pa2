#pragma once
#include <fstream>
#include <string>
#include <algorithm>
#include "BaseException.hpp"

class FileHandler
{
private:
    std::fstream file;
    const std::string END_SEQUENCE = "***END***";

public:

    /**
     * @brief   Tries to open file with give path and name in mode writing
     * @param   _fileName   File to be opened
     * @throw   
     */
    void OpenForWrite(const std::string & _fileName);

    /**
     * @brief   Tries to open file with give path and name in mode reading
     * @param   _fileName   File to be opened
     * @throw   
     */
    void OpenForRead(const std::string & _fileName);

    /**
     * @brief   Close currently opened file
     */
    void Close();

    /**
     * @brief   Read a number from stored text file.
     * @param   _saveFile           Text file from which to read
     * @return  int                 Parsed number 
     * @throw   SaveFileException   Read line contained something else than valid number
     * @throw   std::exception      Getline or stoi failed
     */
    int ReadNumber();

    /**
     * @brief   Read a char from stored text file.
     * @param   _saveFile           Text file from which to read
     * @return  char                 Parsed number 
     * @throw   SaveFileException   Read line contained something else than valid number
     * @throw   std::exception      Getline or stoi failed
     */
    char ReadChar();

    /**
     * @brief   Read a string from stored text file.
     * @param   _saveFile           Text file from which to read
     * @return  string              Parsed string 
     * @throw   SaveFileException   Read line contained something else than valid number
     * @throw   std::exception      Getline or stoi failed
     */
    std::string ReadString();

    /**
     * @brief   Reading an ending sequence from a text file
     */
    void ReadEnd();

    /**
     * @brief   Write a number to stored text file.
     * @param   _number 
     */
    void WriteNumber(int _number);

    /**
     * @brief   Write a char to stored text file.
     * @param   _char 
     */
    void WriteChar(char _char);

    /**
     * @brief   Write a string to stored text file.
     * @param   _string 
     */
    void WriteString(std::string _string);

    /**
     * @brief   Writing an ending sequence to a text file
     */
    void WriteEnd();
};