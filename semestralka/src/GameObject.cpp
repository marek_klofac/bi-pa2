#include "GameObject.hpp"

GameObject::GameObject()
{

}

GameObject::GameObject(int _x, int _y, char _symbol, std::string _encounterText, std::string _interactText):
    position(Position(_x, _y)), symbol(_symbol), encounterText(_encounterText), interactText(_interactText)
{
    
}

Position GameObject::GetPosition()
{
    return position;
}

void GameObject::SetPosition(const Position & _position)
{
    position = _position;
}

std::string GameObject::GetEncounterText()
{
    return encounterText;
}

std::string GameObject::GetInteractText()
{
    return interactText;
}

void GameObject::DrawEncounter(WINDOW * _window)
{
    return;
}

void GameObject::Save(FileHandler & _saveFile)
{
    _saveFile.WriteChar(symbol);
    _saveFile.WriteNumber(position.GetX());
    _saveFile.WriteNumber(position.GetY());
    _saveFile.WriteString(encounterText);
    _saveFile.WriteString(interactText);
}

void GameObject::Load(FileHandler & _saveFile)
{
    position.SetX(_saveFile.ReadNumber());
    position.SetY(_saveFile.ReadNumber());
    encounterText = _saveFile.ReadString();
    interactText = _saveFile.ReadString();
}