#pragma once
#include <string>
#include <iostream>

class BaseException
{
private:
    int code;
    std::string message;

public:
    /**
     * @brief   Construct a new Base Exception object
     * @param   _code       Numeric value of error code 
     * @param   _message    String message describing the problem
     */
    BaseException(int _code, std::string _message);

    /**
     * @brief   Get the message stored int this exception
     * 
     * @return  std::string     Stored message
     */
    std::string GetMessage() const;

    /**
     * @brief                   Overloaded output operator for stream.
     * @param   os              Output stream (std::cout)
     * @param   e               Thrown exception to be printed
     * @return  std::ostream&   Modified stream
     */
    friend std::ostream & operator << (std::ostream & _os, const BaseException & _e);
};

class NoColorTermException: public BaseException
{
public:
    NoColorTermException(std::string _message);
};

class MapFileException: public BaseException
{
public:
    MapFileException(std::string _message);
};

class SaveFileException: public BaseException
{
public:
    SaveFileException(std::string _message);
};

class NonMovableObjectException: public BaseException
{
public:
    NonMovableObjectException(std::string _message);
};

class MapBoundariesException: public BaseException
{
public:
    MapBoundariesException(std::string _message);
};

class InventoryFullException: public BaseException
{
public:
    InventoryFullException(std::string _message);
};

class FileHandlerException: public BaseException
{
public:
    FileHandlerException(std::string _message);
};

class MoveOutOfBoundsException: public BaseException
{
public:
    MoveOutOfBoundsException(std::string _message);
};

class InternalErrorException: public BaseException
{
public:
    InternalErrorException(std::string _message);
};