#pragma once
#include <string>
#include <sstream>
#include <ncurses.h>
#include <memory>
#include "Player.hpp"
#include "GameObject.hpp"

class InfoPanel
{
private:
	std::string title = "Welcome!";
	std::string text = "The game has started.";
	std::shared_ptr<Player> player;
    std::shared_ptr<GameObject> encountered = nullptr;

public:

	/**
	 * Draws the Info panel into the game screen.
	 * @param	window	    Pointer to ncurses window where to draw
	 */
	void Draw(WINDOW * _window);

	/**
	 * Updates displayed title and text values
	 * @param	_title	        New string value of title.
	 * @param	_text	        New string value of text.
     * @param	_encountered	Encountered GameObject.
	 */
	void Update(const std::string & _title, const std::string & _text, const std::shared_ptr<GameObject> & _encountered);

    /**
     * @brief   Set the internal pointer to point to given Player object
     * @param   _player     Player instance to be pointed to in this class
     */
    void SetPlayer(const std::shared_ptr<Player> & _player);

    /**
	 * Clears the Info panel from the game window.
	 * @param   _window	    Pointer to ncurses window where to draw
	 */
	void Clear(WINDOW * _window) const;
};
