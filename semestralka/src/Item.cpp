#include "Item.hpp"

Item::Item(int _x, int _y, std::string _name, int _rarity):
    GameObject(
        _x,
        _y,
        static_cast<char>(Constants::SYMBOLS::ITEM),
        "default item encounter text",
        "default item interact text"
    ),
    rarity(_rarity),
    name(_name)
{
    bonusAttackDamage = std::rand() % rarity;
    bonusAbilityPower = std::rand() % rarity;
    bonusDefense = std::rand() % rarity;
    encounterText = "You have encoutered an ITEM called: " + _name + " | Rarity: " + std::to_string(_rarity);
    interactText = "Item picked up. Bonuses -> Attack damage: +" + std::to_string(bonusAttackDamage) + " | Ability power: +" + std::to_string(bonusAbilityPower) + " | Defense: +" + std::to_string(bonusDefense);
}

Item::Item(FileHandler & _saveFile, bool _standalone)
{
    if(_standalone)
    {
        Load(_saveFile);
    }
    else
    {
        LoadToInventory(_saveFile);
    }
}

void Item::Draw(WINDOW * _window)
{
    mvwaddch(_window, position.GetY(), position.GetX(), symbol | COLOR_PAIR(Constants::NCURSES_COLORS::ITEM));
    refresh();
}

bool Item::Interact(const std::shared_ptr<Player> & _player)
{
    try
    {
        _player->GetInventory().AddItem(std::make_shared<Item>(*this));
    }
    catch (const BaseException & _e)
    {
        interactText = _e.GetMessage();
        return false;
    }
    return true;
}

std::string Item::GetName()
{
    return name;
}

int Item::GetBonusAttackDamage()
{
    return bonusAttackDamage;
}

int Item::GetBonusAbilityPower()
{
    return bonusAbilityPower;
}

int Item::GetBonusDefense()
{
    return bonusDefense;
}

void Item::Save(FileHandler & _saveFile)
{
    GameObject::Save(_saveFile);
    _saveFile.WriteNumber(rarity);
    _saveFile.WriteString(name);
    _saveFile.WriteNumber(bonusAttackDamage);
    _saveFile.WriteNumber(bonusAbilityPower);
    _saveFile.WriteNumber(bonusDefense);
}

void Item::SaveFromInventory(FileHandler & _saveFile)
{
    _saveFile.WriteNumber(rarity);
    _saveFile.WriteString(name);
    _saveFile.WriteNumber(bonusAttackDamage);
    _saveFile.WriteNumber(bonusAbilityPower);
    _saveFile.WriteNumber(bonusDefense);
}

void Item::Load(FileHandler & _saveFile)
{
    symbol = static_cast<char>(Constants::SYMBOLS::ITEM);
    GameObject::Load(_saveFile);
    rarity = _saveFile.ReadNumber();
    name = _saveFile.ReadString();
    bonusAttackDamage = _saveFile.ReadNumber();
    bonusAbilityPower = _saveFile.ReadNumber();
    bonusDefense = _saveFile.ReadNumber();
}

void Item::LoadToInventory(FileHandler & _saveFile)
{
    rarity = _saveFile.ReadNumber();
    name = _saveFile.ReadString();
    bonusAttackDamage = _saveFile.ReadNumber();
    bonusAbilityPower = _saveFile.ReadNumber();
    bonusDefense = _saveFile.ReadNumber();

    symbol = static_cast<char>(Constants::SYMBOLS::ITEM);
    position = Position(-1, -1);
    encounterText = "item in invetory";
    interactText = "item in inventory";
}