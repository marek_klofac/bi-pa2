#pragma once
#include <string>
#include <fstream>
#include <exception>
#include <cstdlib>
#include <ctime>
#include "Scene.hpp"
#include "UIManager.hpp"
#include "Constants.hpp"
#include "BaseException.hpp"
#include "FileHandler.hpp"
#include "Player.hpp"
#include "Wall.hpp"
#include "Item.hpp"
#include "Trap.hpp"
#include "Goblin.hpp"
#include "Dragon.hpp"
#include "Ghost.hpp"

class GameSystem
{
private:
    UIManager ui;
	Scene scene;
    std::ifstream map;		///< Text file containing scene declaration of a map
    FileHandler saveFile;
    bool gameEnded = false;
    bool gameWon = false;

public:
	/**
	 * @brief   Default constructor - intialize internal state of GameSystem object
	 */
	GameSystem();

	/**
	 * @brief   Prints main menu and acts accordingly on user's choice
	 */
	void Start();

	/**
	 * @brief   Starts the main game loop. Cycling until the game is over.
     *          Drawing the scene and asking the user for input each iteration.
     *          This game is turned based, not real-time.
	 */
	void GameLoop();

    /**
	 * @brief   Called from main menu. Brings up "Create player" screen and
     *          updates some of the player's attributes dependent on user's choice.
	 */
    void NewGame();

    /**
     * @brief   Loads scene definition from a text file. Appropriately alocates
     *          all GameObjects dynamically.
     * @param   _mapName     Name of text file from which to load 
     * @throw   e           The map file was corrupted. Unexpected symbols, couldn't be opened.
     */
    void ProcessMap(const std::string & _mapName);

    /**
     * @brief   Checks whether the source map file for New Game is valid or not.
     *          The validity is checked via fixed map width and height and the map
     *          has to be surrounded by Walls (#)
     * @throw   
     */
    void ValidMapBoundaries();

    /**
     * @brief   Returns a random string containing fantasy name
     *          for monsters. Picking from enum in Constants.hpp.
     * @return  std::string     Random fantasy name returned
     */
    std::string RandomFantasyName();

    /**
     * @brief   Returns a random string containing fantasy name
     *          for items. Picking from enum in Constants.hpp.
     * @return  std::string     Random fantasy name returned
     */
    std::string RandomItemName();

	/**
	 * @brief   Saves the current state of the game. Calls virtual save method on
     *          each GameObject in the Scene.
	 */
	void SaveGame();

	/**
	 * @brief   Loads previosly saved state of the game. Calls virtual load method on
     *          each GameObject in the Scene. File is binary.
	 */
	void LoadGame();

	/**
	 * @brief   Draws Option screen into the game window.
	 */
	void Options();

	/**
	 * @brief   Called after game state requires the game to end. Cleanup.
	 */
	void Quit();

	/**
	 * @brief 	Called after an exception is caught. Ends window and cleans up.
	 * @param 	e	Thrown exception to be printed.
	 */
	void ForcedQuit(const BaseException & _e);

	/**
	 * @brief   This method decides, what should happen next, after the user presses a key.
	 * @param	_pressedKey		Integer value of key that was just pressed by the user.
	 */
	void KeyResolve(int _pressedKey);
};
