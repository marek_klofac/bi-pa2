#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <ncurses.h>
#include <memory>
#include <map>
#include "InfoPanel.hpp"
#include "GameObject.hpp"
#include "Character.hpp"
#include "Position.hpp"
#include "BaseException.hpp"
#include "FileHandler.hpp"
#include "Player.hpp"
#include "Wall.hpp"
#include "Item.hpp"
#include "Trap.hpp"
#include "Goblin.hpp"
#include "Dragon.hpp"
#include "Ghost.hpp"

class Scene
{
private:
    /**
     * @brief       Comparison for two coordinate values using functor.
     *              Used in map for internal sorting.
     */
    class PositionCompare
    {
        public:
            /**
            * @brief        Overloaded operator () for comparison
            * @param lhs    Left hand operand 
            * @param rhs    Right hand operand
            * @return       True if lhs is smaller than rhs. First comparing by X axis,
            *               if they are equal, compare by Y axis.
            */
            bool operator ()(const Position & _lhs, const Position & _rhs) const
            {
                if(_lhs.GetX() == _rhs.GetX())
                {
                    return _lhs.GetY() < _rhs.GetY();
                }
                return _lhs.GetX() < _rhs.GetX();
            }
    };
    /**
     * @brief   Polymorphic map containing all objects, that can these
     *          methods can be called on them: Draw, Interact with player, Save, Load
     *          Contains also Player!
     */
    std::map<Position, std::shared_ptr<GameObject>, PositionCompare> scene;

    /**
     * @brief   Polymorphic map containing all objects, that can these
     *          methods can be called on them: Move, Fight
     *          Doesn't contain Player!
     */
    std::map<Position, std::shared_ptr<Character>, PositionCompare> npcs;   // Move, 

    /**
     * @brief   Pointer to single player instance. 
     */
    std::shared_ptr<Player> player;

    /**
     * @brief   Pointer to GameObject that the Player has collided with.
     */
    std::shared_ptr<GameObject> encountered = nullptr;

public:

    /**
     * @brief   Checks whether given Position is valid in given map
     * @param   _position   Position to be checked
     * @throw   MoveOutOfBoundsException    Invalid position
     */
    void ValidPosition(const Position & _position);
    
    /**
	 * Draws an actual state of the scene into the window.
     * @param   _window     Pointer to window where to draw.
	 */
    void Draw(WINDOW * _window);

    /**
     * @brief   Called everytime user presses a key on his keyboard.
     *          
     * @param   _pressedKey     User's input (ncurses constant)
     * @param   _window         Ncurses window pointer where encoutered GameObject writes it's stats
     * @param   _infoPanel      UI's info panel to display current info to
     */
    void Movement(int _pressedKey, InfoPanel & _infoPanel);

    /**
     * @brief   Called after user pressed an 'E' on his keyboard. Solving
     *          any scene interaction of Player with another GameObject
     * @param   _window     Ncurses window pointer 
     * @param   _infoPanel  UI's info panel to display current info to
     * @return  true        Player died during the interaction
     * @return  false       Player survived the interaction
     */
    bool Interaction(InfoPanel & _infoPanel);

	/**
     * @return Shared pointer to player instance. Can be modified.
	 */
	std::shared_ptr<Player> GetPlayer();

    /**
     * @brief   Registers new GameObject into the scene. Allocating new
     *          object on heap as a copy of obtained object.
     * @param   _object     GameObject reference to allocate on heap and add to scene
     */
    void AddGameObject(const std::shared_ptr<GameObject> & _object);

    /**
     * @brief   Registers new Character into the scene. Allocating new
     *          object on heap as a copy of obtained object.
     * @param   _object     GameObject reference to allocate on heap and add to scene
     */
    void AddCharacter(const std::shared_ptr<Character> & _object);

    /**
     * @brief   Updates the internal smart pointer to point
     *          to newly instantiated player object and adds
     *          pointer to player also to coordinate map and the scene
     * @param   _player     Newly allocated player object 
     */
    void AddPlayer(const std::shared_ptr<Player> & _player);

    /**
	 * Calls save method on each GameObject that is stored in current Scene.
     * @throw   std::ofstream::failure
	 */
	void Save(FileHandler & _saveFile);

	/**
	 * Calls load method on each GameObject that is stored in current Scene.
     * @throw   std::ifstream::failure
	 */
	void Load(FileHandler & _saveFile);

    /**
     * @brief   Informs GameSystem about the state of npcs count
     * @return  true    Npcs map is empty, player killed all of them
     * @return  false   Some npcs are still alive
     */
    bool NoEnemiesLeft();
};
