#pragma once
#include <string>
#include <map>
#include "Player.hpp"

class Goblin: public Character
{
private:
    std::string name;
    enum DEFAULT_STATS
    {
        HEALTH        = 50,
        ATTACK_DAMAGE = 5,
        ABILITY_POWER = 5,
        DEFENSE       = 10
    };

    /**
     * @brief   Calculation of distance of two points [x,y] in 2D space
     * @param   _goblin     Goblin's position
     * @param   _player     Player's position
     * @return  double      Distance calculated via Pythagorean theorem
     */
    double Distance(const Position & _goblin, const Position & _player);

public:

    /**
     * @brief   Construct a new Goblin object using given coordinates
     * @param   _x      X coordinate 
     * @param   _y      Y coordinate
     * @param   _name   Name of the monster
     */
    Goblin(int _x, int _y, std::string _name);

    /**
     * @brief   Construct a new Goblin object using a save file (loading)
     * @param   _saveFile   Save file from which to load and initialize variables
     */
    Goblin(FileHandler & _saveFile);

    /**
	 * @brief	Goblin draws its symbol to window at current position
	 * @param 	_window 
	 */
	void Draw(WINDOW * _window) override;

    /**
     * @brief   Display current goblin's stats into nCurses window
     * @param   _window     Window pointer where to display
     */
    void DrawEncounter(WINDOW * _window) override;

    /**
     * @brief   This method is called, when something encounters Goblin.
     *          E.g. Player encouters this Goblin
     * @param   _other    Pointer to GameObject, that collided with Goblin
     * @return  true    GameObject should be destroyed after this interaction
     * @return  false   GameObject should not be destroyed after this interaction
     */
    bool Interact(const std::shared_ptr<Player> & _player) override;

    /**
     * @brief   Virtual method for saving the current state of Goblin.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    void Save(FileHandler & _saveFile) override;

    /**
     * @brief   Virtual method for loading previously saved state of Goblin.
     *          Each class that derives from this should also know how to 
     *          load it's own values from this text file
     * @param   _saveFile   Text file where to store data
     */
    void Load(FileHandler & _saveFile) override;

    /**
     * @brief   Calculating new position, where Goblin would move to and returning it.
     *          Goblin is chasing player, using Pythagorean theorem for calculation
     *          of shortest path to Player's position
     * @return  Position        Newly calculated position, where the Goblin would move to
     */
    Position NextPosition(const Position & _playerPosition) override;

    /**
     * @brief   Returns the value of how much the attack
     *          is powerful. The less health the goblin has
     *          the lower the value of attack is
     * @return  int     Attack power all together 
     */
    int CalculateAttack() override;

    /**
     * @brief   Goblin takes damage appropriately,
     *          taking into account it's defenses
     * @param   _damage     Pure damage given by other Character
     */
    void TakeDamage(int _damage) override;
};