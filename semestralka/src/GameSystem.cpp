#include "GameSystem.hpp"

using namespace std;

GameSystem::GameSystem()
{
    std::srand(std::time(nullptr));
}

void GameSystem::Start()
{
    ui.DrawMainMenu();

    // Wait for user input and act accordingly
    UIManager::MenuChoices choice = ui.GetMainMenuChoice();
	try
	{
		switch(choice)
		{
			case UIManager::MenuChoices::NewGame:
				NewGame();
				break;
			case UIManager::MenuChoices::LoadGame:
				LoadGame();
				break;
			case UIManager::MenuChoices::Options:
				Options();
				break;
			case UIManager::MenuChoices::Quit:
				Quit();
				break;
            default:
                break;
		}
	}
	catch(const BaseException & e)
    {
		ForcedQuit(e);
		return;
    }
}

void GameSystem::GameLoop()
{
	while(!gameEnded)
	{
        // Clear window & draw actual state of scene
        ui.ClearWindow();
        scene.Draw(ui.GetWindow());

        // Draw actual state of other UI elements
        ui.DrawInfoPanel();

        // Get user's input
		int pressedKey = wgetch(ui.GetWindow());

        // Act upon it
        KeyResolve(pressedKey);
	}
}

void GameSystem::NewGame()
{
    // Loading map file specified in constants file.
    // Going through text file containing player, monsters, walls etc
    ProcessMap(Constants::MAP_FILE);

    // As of now, player is in default state. Change its modifiable values
    // dependent on user's input
    ui.DrawCreatePlayer(scene.GetPlayer());

    // Send into the game
    GameLoop();
}

void GameSystem::ProcessMap(const std::string & _mapName)
{
    map.open(_mapName);
	if(!map.good())
	{
		throw(MapFileException("Map file either doesn't exist or couldn't be opened."));
	}
    // Report any faults in map boundaries (has to be surrounded by walls #)
    ValidMapBoundaries();
    
    // Process text file line by line
    std::string line;

    // Starting coordinates from (1,1) because of ncurses box
    int yCoor = 1;
    while(std::getline(map, line))
    {
        // Proccess each line char by char
        int xCoor = 1;
        for(auto & c: line)
        {
            // Instantiation of different GameObjects
            switch(c)
            {
                case static_cast<char>(Constants::SYMBOLS::PLAYER):
                    scene.AddPlayer(std::make_shared<Player>(xCoor, yCoor));
                    break;
                case static_cast<char>(Constants::SYMBOLS::WALL):
                    scene.AddGameObject(std::make_shared<Wall>(xCoor, yCoor));
                    break;
                case static_cast<char>(Constants::SYMBOLS::ITEM):
                    scene.AddGameObject(std::make_shared<Item>(xCoor, yCoor, RandomItemName(), std::rand() % Constants::MAX_ITEM_RARITY + 1));
                    break;
                case static_cast<char>(Constants::SYMBOLS::TRAP):
                    scene.AddGameObject(std::make_shared<Trap>(xCoor, yCoor));
                    break;
                case static_cast<char>(Constants::SYMBOLS::GOBLIN):
                    scene.AddCharacter(std::make_shared<Goblin>(xCoor, yCoor, RandomFantasyName()));
                    break;
                case static_cast<char>(Constants::SYMBOLS::DRAGON):
                    scene.AddCharacter(std::make_shared<Dragon>(xCoor, yCoor, RandomFantasyName()));
                    break;
                case static_cast<char>(Constants::SYMBOLS::GHOST):
                    scene.AddCharacter(std::make_shared<Ghost>(xCoor, yCoor, RandomFantasyName()));
                    break;
                // Empty space, draw nothing
                case ' ':
                    break;
                default:
                    throw(MapFileException("Corrupted map file. It contains unprocessable symbols."));
            }
            xCoor++;
        }
        yCoor++;
    }
    if(scene.GetPlayer() == nullptr)
    {
        throw(MapFileException("Corrupted map file. Map has to contain atleast a player!"));
    }
}

void GameSystem::ValidMapBoundaries()
{
    std::string line;
    int height = 0;
    while(std::getline(map, line))
    {
        // Wrong width
        if(line.size() != Constants::MAP_WIDTH)
        {
            throw(MapBoundariesException("Invalid width of map's row. Check default_map file.\nBe careful on Windows, \\r\\n could appear (should be only \\n). -> \"sed \'s/\\r$//\'\""));
        }
        // First line or last line encountered, check if all are walls #
        if(height == 0 || height == Constants::MAP_HEIGHT - 1)
        {
            for(auto & c: line)
            {
                if(c != static_cast<char>(Constants::SYMBOLS::WALL))
                {
                    throw(MapBoundariesException("Invalid first or last map's row. Has to be all Walls (#).\nCheck default_map file."));
                }
            }
        }
        // Some line in between encoutered, check if starts and ends with #
        if(line.front() != static_cast<char>(Constants::SYMBOLS::WALL) || line.back() != static_cast<char>(Constants::SYMBOLS::WALL))
        {
            throw(MapBoundariesException("Invalid map's row. Has to start and end with Wall (#).\nCheck default_map file."));
        }
        // Jump to next line
        height++;
    }
    // Wrong height
    if(height != Constants::MAP_HEIGHT)
    {
        throw(MapBoundariesException("Map file has too many or too few rows."));
    }
    // Jump to beginning a reset the file
    map.clear();
    map.seekg(0, std::ios::beg);
}

std::string GameSystem::RandomFantasyName()
{
    return Constants::FANTASY_NAMES[std::rand() % Constants::FANTASY_NAMES.size()];
}

std::string GameSystem::RandomItemName()
{
    return Constants::ITEM_NAMES[std::rand() % Constants::ITEM_NAMES.size()];
}

void GameSystem::SaveGame()
{
	// Open save file for writing
    saveFile.OpenForWrite(Constants::SAVE_FILE);
    scene.Save(saveFile);
    saveFile.WriteEnd();
	saveFile.Close();
}

void GameSystem::LoadGame()
{
	// Open save file for reading
    saveFile.OpenForRead(Constants::SAVE_FILE);
    try
    {
        scene.Load(saveFile);
        saveFile.ReadEnd();
    }
    catch (const std::exception & _e)
    {
        throw(SaveFileException("Save file corrupted or modified. sdt::exception thrown (stoi)"));
    }
	saveFile.Close();

    // Update UI's player pointer
    ui.GetInfoPanel().SetPlayer(scene.GetPlayer());

    // Send user to game
	GameLoop();
}

void GameSystem::Options()
{
    // Continue after user hits 'esc' button
	ui.DrawOptions();

    // Jump back to main menu
	Start();
}

void GameSystem::Quit()
{
	gameEnded = true;
}

void GameSystem::ForcedQuit(const BaseException & _e)
{
	ui.CloseWindow();
	std::cout << std::endl << _e << std::endl;
}

void GameSystem::KeyResolve(int _pressedKey)
{
    // 'W', 'S', 'A', 'D' - ncurses constants
	if (_pressedKey == KEY_UP || _pressedKey == KEY_DOWN || _pressedKey == KEY_LEFT || _pressedKey == KEY_RIGHT)
	{
        // Movement started. Update the whole scene
        scene.Movement(_pressedKey, ui.GetInfoPanel());
	}
    // 'ESC' - open up in-game menu
	else if (_pressedKey == 27)
	{
        ui.DrawInGameMenu();
	    UIManager::MenuChoices choice = ui.GetInGameMenuChoice();
        switch(choice)
        {
            case UIManager::MenuChoices::SaveGame:
                SaveGame();
                break;

            case UIManager::MenuChoices::Quit:
                Quit();
                break;

            default:
                break;
        }
	}
    // 'E' - interact with encoutered GameObject
	else if (_pressedKey == 101)
	{
        // Check if player after interaction died
		if(scene.Interaction(ui.GetInfoPanel()))
        {
            ui.DrawEndScreen();
            Quit();
        }
        // Check if all npcs after interaction are dead -> Won game!
        if(scene.NoEnemiesLeft())
        {
            ui.DrawWinScreen();
            Quit();
        }
	}
}
