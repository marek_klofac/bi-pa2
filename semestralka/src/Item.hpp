#pragma once
#include "GameObject.hpp"
#include "Player.hpp"

class Item: public GameObject
{
private:
    int rarity;
    std::string name;
    int bonusAttackDamage;
    int bonusAbilityPower;
    int bonusDefense;

public:
    /**
     * @brief   Construct a new Item object using given coordinates
     * @param   _x          X coordinate 
     * @param   _y          Y coordinate
     * @param   _name       Name of the item
     * @param   _rarity     Rarity of an item
     */
    Item(int _x, int _y, std::string _name, int _rarity);

    /**
     * @brief   Construct a new Item object using a save file (loading)
     * @param   _saveFile   Save file from which to load and initialize variables
     * @param   _standalone True - can exists in the world (valid position, symbol, etc.)
                            False - player has it in his inventory, has valid only Item's variables
     */
    Item(FileHandler & _saveFile, bool _standalone);

    /**
	 * @brief	Item draws its symbol to window at current position
	 * @param 	_window 
	 */
	void Draw(WINDOW * _window) override;

    /**
     * @brief   This method is called, when something encounters Item.
     *          E.g. Player encouters this Item
     * @param   _other  Pointer to GameObject, that collided with Goblin
     * @return  true    GameObject should be destroyed after this interaction
     * @return  false   GameObject should not be destroyed after this interaction
     */
    bool Interact(const std::shared_ptr<Player> & _player) override;

    /**
     * @brief   Get the name of this item
     * @return  std::string     Name of the item
     */
    std::string GetName();

    /**
     * @brief   Get the bonus attack damage of this item
     * @return  int     Bonus
     */
    int GetBonusAttackDamage();
    
    /**
     * @brief   Get the bonus ability power of this item
     * @return  int     Bonus
     */
    int GetBonusAbilityPower();

    /**
     * @brief   Get the bonus defense of this item
     * @return  int     Bonus
     */
    int GetBonusDefense();

    /**
     * @brief   Virtual method for saving the current state of Item.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    virtual void Save(FileHandler & _saveFile) override;

    /**
     * @brief   Saving the current variables of an item into a text file
     *          Note that this method should be only called, when the item
     *          is inside an inventory. E.g. not saving symbol or position of GameObject.
     * @param   _saveFile   File where to save
     */
    void SaveFromInventory(FileHandler & _saveFile);

    /**
     * @brief   Virtual method for laoding previously saved state of Item.
     *          Each class that derives from this should also know how to 
     *          load it's own values into a text file
     * @param   _saveFile   Text file from where to load data
     */
    virtual void Load(FileHandler & _saveFile) override;

    /**
     * @brief   Loading the previously stored variables of an item from a text file
     *          Note that this method should be only called, when the item
     *          is going to be in an inventory. E.g. not loading symbol or position of GameObject.
     * @param   _saveFile   File from which to load
     */
    void LoadToInventory(FileHandler & _saveFile);
};