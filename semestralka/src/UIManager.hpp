#pragma once
#include <string>
#include <memory>
#include <ncurses.h>
#include "Player.hpp"
#include "BaseException.hpp"
#include "Constants.hpp"
#include "InfoPanel.hpp"

class UIManager
{
private:
	const int WINDOW_HEIGHT = 36;
	const int WINDOW_WIDTH = 120;
	WINDOW * window;
    InfoPanel infoPanel;
    std::string MenuChoicesForDraw[4] = {"New game","Load game","Options","Quit game"};
    std::string InGameMenuChoicesForDraw[2] = {"Save game", "Quit game"};

public:
    enum MenuChoices{NewGame, LoadGame, Options, Quit, SaveGame};

	/**
	 * @brief   Allocates ncurses window and initializes it's context.
	 */
	UIManager();
	
    /**
	 * @brief   Deletes the window and frees up ncurses context.
	 */
	~UIManager();

	/**
	 * @brief   This function return a valid pointer to ncruses window
	 * @return  Pointer to ncurses window.
	 */
	WINDOW * GetWindow();

    /**
     * @brief   Get the Info Panel object
     * @return  UI's InfoPanel
     */
    InfoPanel & GetInfoPanel();

    /**
	 * @brief   Draws welcome screen into the window.
	 */
	void DrawWelcomeScreen() const;

	/**
	 * @brief   Draws main menu into the window.
	 */
	void DrawMainMenu() const;

    /**
	 * @brief   Draws in game menu into the window.
	 */
	void DrawInGameMenu() const;

    /**
	 * @brief   Reacting on user's keyboard input. Elements in menu can be
     *          iterated over and are highlighted.
     * @return  User's choice, later used in GameSystem for what to do next
	 */
	MenuChoices GetMainMenuChoice() const;

    /**
	 * @brief   Reacting on user's keyboard input. Elements in menu can be
     *          iterated over and are highlighted. This in game menu is
     *          brought up mid game if user pressed 'ESC' key.
     * @return  User's choice, later used in GameSystem for what to do next
	 */
	MenuChoices GetInGameMenuChoice() const;

	/**
	 * @brief   Draws options into window. User can get back into main menu
     *          by pressing 'ESC' button.
	 */
	void DrawOptions () const;

    void DrawInfoPanel();

    /**
     * @brief   Draws character customization screen to the window and
	 *          updates obtained player instance with user's inputs.
     * @param	_player		Pointer to player instance to be updated
	 */
    void DrawCreatePlayer(const std::shared_ptr<Player> & _player);

	/**
	 * @brief   Prints Death screen to the window.
	 */
	void DrawEndScreen() const;

    /**
     * @brief   Prints victory screen to the window
     */
    void DrawWinScreen() const;

	/**
	 * @brief Closes ncurses window.
	 */
	void CloseWindow();

    /**
     * @brief Clears ncurses window and restores box around
     */
    void ClearWindow() const;
};
