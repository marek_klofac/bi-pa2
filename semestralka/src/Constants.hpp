#pragma once
#include <string>
#include <vector>

namespace Constants
{
    const std::string MAP_FILE = "src/maps/map1.txt";
    const int MAP_HEIGHT = 28;
    const int MAP_WIDTH = 98;
    const std::string SAVE_FILE = "src/save/current_save";
    const int MAX_ITEM_RARITY = 10;
    enum NCURSES_COLORS
    {
        PLAYER  = 1,
        WALL    = 2,
        ITEM    = 3,
        TRAP    = 4,
        GOBLIN  = 5,
        DRAGON  = 6,
        GHOST   = 7,
        BONUS   = 8
    };
    enum class SYMBOLS
    {
        PLAYER  = '@',
        WALL    = '#',
        ITEM    = '$',
        TRAP    = '&',
        GOBLIN  = 'G',
        DRAGON  = 'D',
        GHOST   = 'H'
    };
    const std::vector<std::string> FANTASY_NAMES =
    {
        "Baldur",
        "Ragnar",
        "Yuga",
        "Vervalla",
        "Berolan",
        "Eridor",
        "Gretian",
        "Frundor",
        "Combe",
        "Presz",
        "Aard",
        "Pird",
        "Feg",
        "Zrolorx",
        "Stebkylk",
        "Praatiol",
        "Alwin",
        "Empyr",
        "Blase",
        "Rayth",
        "Desir",
        "Samhayn",
        "Curce",
        "Azazel",
        "Oroan",
        "Otis",
        "Onoskelis",
        "Nahas",
        "Negal",
        "Nuberu",
        "Irritium",
        "Balam",
        "Bali Raj",
        "Astaroth",
        "Caim",
        "Abraxas",
        "Pazuzu",
        "Orthon"
    };
    const std::vector<std::string> ITEM_NAMES =
    {
        "Runed Fruit",
        "Slumber Amulet",
        "All-Seeing anchor",
        "Restoration Ichor",
        "Wisdom Stone",
        "Robes of Grace",
        "Cloak of Destiny",
        "Arch of Treachery",
        "Torture Robes",
        "Rogue Root",
        "Dominance Goblet",
        "Statue of Revival",
        "Fountain of Luck",
        "Grail of Decay",
        "Hellish Goblet",
        "Shield of Light",
        "Mantle of Passion",
        "Holy Longsword",
        "Infused Quickblade",
        "Protector Guardian",
        "Dawnbreaker",
        "Malicious Scimitar",
        "Oathkeeper"
    };
}