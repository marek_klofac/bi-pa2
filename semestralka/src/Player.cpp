#include "Player.hpp"

Player::Player(int _x, int _y):
    Character(
        _x,
        _y,
        static_cast<char>(Constants::SYMBOLS::PLAYER),
        "player encounter text",
        "player interact text",
        DEFAULT_STATS::HEALTH,
        DEFAULT_STATS::ATTACK_DAMAGE,
        DEFAULT_STATS::ABILITY_POWER,
        DEFAULT_STATS::DEFENSE
    ),
    experience(0),
    rotation(KEY_UP)
{}

Player::Player(FileHandler & _saveFile)
{
    Load(_saveFile);
}

void Player::Draw(WINDOW * _window)
{
    mvwaddch(_window, position.GetY(), position.GetX(), symbol | COLOR_PAIR(Constants::NCURSES_COLORS::PLAYER));
    inventory.Draw(_window);
    refresh();
}

void Player::DrawEncounter(WINDOW * _window)
{
    mvwprintw(_window, 29, 85, "|");
    wattron(_window, A_UNDERLINE);
    mvwprintw(_window, 29, 87, "PLAYER:");
    wattroff(_window, A_UNDERLINE);
    mvwprintw(_window, 30, 85, "| Health:        %d", health);
    mvwprintw(_window, 31, 85, "| Attack damage: %d", attackDamage);
    mvwprintw(_window, 32, 85, "| Ability power: %d", abilityPower);
    mvwprintw(_window, 33, 85, "| Defense:       %d", defense);
    mvwprintw(_window, 34, 85, "| Experience:    %d", experience);
    refresh();
}

bool Player::Interact(const std::shared_ptr<Player> & _player)
{
    return false;
}

void Player::Save(FileHandler & _saveFile)
{
    Character::Save(_saveFile);
    _saveFile.WriteNumber(experience);
    _saveFile.WriteNumber(rotation);
    inventory.Save(_saveFile);
}

void Player::Load(FileHandler & _saveFile)
{
    symbol = static_cast<char>(Constants::SYMBOLS::PLAYER);
    Character::Load(_saveFile);
    experience = _saveFile.ReadNumber();
    rotation = _saveFile.ReadNumber();
    inventory.Load(_saveFile);
}

Position Player::NextPosition(const Position & _playerPosition)
{
    switch(rotation)
    {
        case KEY_UP:
            return Position(position.GetX(), position.GetY() - 1);
        case KEY_DOWN:
            return Position(position.GetX(), position.GetY() + 1);
        case KEY_LEFT:
            return Position(position.GetX() - 1, position.GetY());
        case KEY_RIGHT:
            return Position(position.GetX() + 1, position.GetY());
        default:
            return position;
    }
}

int Player::CalculateAttack()
{
    return attackDamage < abilityPower ? abilityPower + inventory.GetBonusAbilityPower() : attackDamage + inventory.GetBonusAttackDamage();
}

void Player::TakeDamage(int _damage)
{
    // Take into account defense and items
    health -= _damage * (1 - ((defense + inventory.GetBonusDefense()) / 100));
}

void Player::SetUIBuffs(int _strength, int _intelligence, int _stamina)
{
    attackDamage += _strength;
    abilityPower += _intelligence;
    defense += _stamina;
}

int Player::GetExperience()
{
    return experience;
}

void Player::GainExperience(int _xp)
{
    experience += _xp;
}

void Player::LevelUp()
{
    health = 100;
    attackDamage += 5;
    abilityPower += 5;
    defense += 5;
    experience = 0;
}

void Player::SetRotation(int _rotation)
{
    rotation = _rotation;
}

Inventory & Player::GetInventory()
{
    return inventory;
}