#include "Goblin.hpp"

double Goblin::Distance(const Position & _goblin, const Position & _player)
{
    return std::sqrt(std::pow(_player.GetX() - _goblin.GetX(), 2) + std::pow(_player.GetY() - _goblin.GetY(), 2));    
}

Goblin::Goblin(int _x, int _y, std::string _name):
    Character(
        _x,
        _y,
        static_cast<char>(Constants::SYMBOLS::GOBLIN),
        "You have encoutered a GOBLIN called: " + _name,
        "default goblin interact text",
        DEFAULT_STATS::HEALTH,
        DEFAULT_STATS::ATTACK_DAMAGE,
        DEFAULT_STATS::ABILITY_POWER,
        DEFAULT_STATS::DEFENSE
    ),
    name(_name)
{}

Goblin::Goblin(FileHandler & _saveFile)
{
    Load(_saveFile);
}

void Goblin::Draw(WINDOW * _window)
{
    mvwaddch(_window, position.GetY(), position.GetX(), symbol | COLOR_PAIR(Constants::NCURSES_COLORS::GOBLIN));
    refresh();
}

void Goblin::DrawEncounter(WINDOW * _window)
{
    mvwprintw(_window, 29, 55, "|");
    wattron(_window, A_UNDERLINE);
    mvwprintw(_window, 29, 57, "GOBLIN:");
    wattroff(_window, A_UNDERLINE);
    mvwprintw(_window, 30, 55, "| Name: %s", name.c_str());
    mvwprintw(_window, 31, 55, "| Health:        %d", health);
    mvwprintw(_window, 32, 55, "| Attack damage: %d", attackDamage);
    mvwprintw(_window, 33, 55, "| Ability power: %d", abilityPower);
    mvwprintw(_window, 34, 55, "| Defense:       %d", defense);
}

bool Goblin::Interact(const std::shared_ptr<Player> & _player)
{
    // Deal damage to Player that encoutered this
    _player->TakeDamage(CalculateAttack());

    // Take damage from Player that encoutered this
    this->TakeDamage(_player->CalculateAttack());

    if(health <= 0)
    {
        int xp = (std::rand() % 15) + 1;
        _player->GainExperience(xp);
        interactText = "Goblin killed, gained " + std::to_string(xp) + " XP";
        return true;
    }
    else
    {
        interactText = "SLASH! Fight in progress, check health values.";
        return false;
    }
}

void Goblin::Save(FileHandler & _saveFile)
{
    Character::Save(_saveFile);
    _saveFile.WriteString(name);
}

void Goblin::Load(FileHandler & _saveFile)
{
    symbol = static_cast<char>(Constants::SYMBOLS::GOBLIN);
    Character::Load(_saveFile);
    name = _saveFile.ReadString();
}

Position Goblin::NextPosition(const Position & _playerPosition)
{
    // 25% that Goblin will not move at all
    if(std::rand() % 4 == 0)
    {
        return position;
    }
    // Calculating all 4 possible distances from current Goblin's position to Player's
    std::map<double, Position> distances;
    distances.insert(std::make_pair(Distance(Position(position.GetX(), position.GetY() - 1), _playerPosition), Position(position.GetX(), position.GetY() - 1)));
    distances.insert(std::make_pair(Distance(Position(position.GetX(), position.GetY() + 1), _playerPosition), Position(position.GetX(), position.GetY() + 1)));
    distances.insert(std::make_pair(Distance(Position(position.GetX() - 1, position.GetY()), _playerPosition), Position(position.GetX() - 1, position.GetY())));
    distances.insert(std::make_pair(Distance(Position(position.GetX() + 1, position.GetY()), _playerPosition), Position(position.GetX() + 1, position.GetY())));

    // Return the position, that has the shortest distance to Player
    return distances.begin()->second;
}

int Goblin::CalculateAttack()
{
    // The more the goblin is hurt the less he deals damage
    return pow(attackDamage, health / DEFAULT_STATS::HEALTH);
}

void Goblin::TakeDamage(int _damage)
{
    health -= _damage * (1 - (defense / 100.0));
}