#include "Scene.hpp"

void Scene::ValidPosition(const Position & _position)
{
    if((_position.GetX() < 0 || _position.GetX() > Constants::MAP_WIDTH)
    || (_position.GetY() < 0 || _position.GetY() > Constants::MAP_HEIGHT))
    {
        throw(MoveOutOfBoundsException("Trying to move with Character out of map"));
    }
}

void Scene::Draw(WINDOW * _window)
{
    player->Draw(_window);
    for(auto const & it: scene)
    {
        it.second->Draw(_window);
    }
}

void Scene::Movement(int _pressedKey, InfoPanel & _infoPanel)
{
    // Player movement
    player->SetRotation(_pressedKey);
    Position newPosition = player->NextPosition(player->GetPosition());
    try
    {
        ValidPosition(newPosition);
    }
    catch(const BaseException & _e)
    {
        // Let the player to try to move somewhere else
        _infoPanel.Update("EXCEPTION", _e.GetMessage(), encountered);
        return;
    }
    auto collision = scene.find(newPosition);

    // Trying to move to empty place, allow it
    if(collision == scene.end())
    {
        encountered = nullptr;
        _infoPanel.Update("MOVE", "Moving to an empty space.", encountered);
        // Update new position in scene
        auto it1 = scene.extract(player->GetPosition());
        player->SetPosition(newPosition);
        it1.key() = newPosition;
        scene.insert(std::move(it1));
    }
    else
    {
        encountered = collision->second;
        _infoPanel.Update("ENCOUNTER", encountered->GetEncounterText() + "\n\nPress 'E' to interact.", encountered);
    }
    // NPCs movement
    for(auto & [oldPosition, npc]: npcs)
    {
        // Obtain GameObject's next move and find any collisions
        Position newPosition = npc->NextPosition(player->GetPosition());
        try
        {
            ValidPosition(newPosition);
        }
        catch(const BaseException & _e)
        {
            // Skip this monsters movement
            continue;
        }

        auto collision = scene.find(newPosition);
        if(collision == scene.end())
        {
            npc->SetPosition(newPosition);

            // Update new position in scene & npcs
            auto it1 = scene.extract(oldPosition);
            it1.key() = newPosition;
            scene.insert(std::move(it1));
            auto it2 = npcs.extract(oldPosition);
            it2.key() = newPosition;
            npcs.insert(std::move(it2));
        }
    }
}

bool Scene::Interaction(InfoPanel & _infoPanel)
{
    // User pressed 'E' for interaction but hasn't encountered anything
    if(encountered == nullptr)
    {
        _infoPanel.Update("INTERACT", "Nothing to do here...", encountered);
        return false;
    }
    // Encountered GameObject by Player interacts with him
    else
    {
        // GameObject should be removed from the scene (e.g monster killed)
        if(encountered->Interact(player))
        {
            _infoPanel.Update("INTERACT", encountered->GetInteractText(), encountered);
            scene.erase(encountered->GetPosition());
            npcs.erase(encountered->GetPosition());
            encountered = nullptr;
            // Player new level
            if(player->GetExperience() >= 100)
            {
                player->LevelUp();
                _infoPanel.Update("LEVEL UP", "Your basic stats has been increased!", encountered);
            }
        }
        else
        {
            _infoPanel.Update("INTERACT", encountered->GetInteractText() + "\n\nPress 'E' continue interaction.", encountered);
        }
        if(player->GetHealth() <= 0)
        {
            return true;
        }
    }
    return false;
}

std::shared_ptr<Player> Scene::GetPlayer()
{
    return player;
}

void Scene::AddGameObject(const std::shared_ptr<GameObject> & _object)
{
    scene.insert(make_pair(_object->GetPosition(), _object));
}

void Scene::AddCharacter(const std::shared_ptr<Character> & _object)
{
    npcs.insert(make_pair(_object->GetPosition(), _object));

    // Also add to scene
    AddGameObject(_object);
}

void Scene::AddPlayer(const std::shared_ptr<Player> & _player)
{
    player = _player;
    AddGameObject(_player);
}

void Scene::Save(FileHandler & _saveFile)
{
    _saveFile.WriteNumber(scene.size());
    for(auto & it: scene)
    {
        it.second->Save(_saveFile);
    }
}

void Scene::Load(FileHandler & _saveFile)
{
    unsigned int savedGameObjects = _saveFile.ReadNumber();
    for(unsigned int i = 0; i < savedGameObjects; i++)
    {
        char symbol = _saveFile.ReadChar();
        switch(symbol)
        {
            case static_cast<char>(Constants::SYMBOLS::PLAYER):
                AddPlayer(std::make_shared<Player>(_saveFile));
                break;
            case static_cast<char>(Constants::SYMBOLS::WALL):
                AddGameObject(std::make_shared<Wall>(_saveFile));
                break;
            case static_cast<char>(Constants::SYMBOLS::ITEM):
                AddGameObject(std::make_shared<Item>(_saveFile, true));
                break;
            case static_cast<char>(Constants::SYMBOLS::TRAP):
                AddGameObject(std::make_shared<Trap>(_saveFile));
                break;
            case static_cast<char>(Constants::SYMBOLS::GOBLIN):
                AddCharacter(std::make_shared<Goblin>(_saveFile));
                break;
            case static_cast<char>(Constants::SYMBOLS::DRAGON):
                AddCharacter(std::make_shared<Dragon>(_saveFile));
                break;
            case static_cast<char>(Constants::SYMBOLS::GHOST):
                AddCharacter(std::make_shared<Ghost>(_saveFile));
                break;
            default:
                throw(SaveFileException("Save file corrupted or modified. Read char is not a valid symbol."));
        }
    }
    if(scene.size() != savedGameObjects)
    {
        throw(SaveFileException("Save file corrupted or modified. Too many or too few game objects."));
    }
    if(player == nullptr)
    {
        throw(SaveFileException("Save file corrupted or modified. Player not loaded."));
    }
}

bool Scene::NoEnemiesLeft()
{
    return npcs.size() == 0;
}