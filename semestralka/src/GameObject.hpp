#pragma once
#include <iostream>
#include <ncurses.h>
#include <string>
#include <memory>
#include "Position.hpp"
#include "Constants.hpp"
#include "BaseException.hpp"
#include "FileHandler.hpp"

// Forward declaration to fix circular dependecy
class Player;

class GameObject
{
protected:
    Position position;
    char symbol;
    std::string encounterText;
    std::string interactText;

public:

    /**
     * @brief   Construct a empty GameObject object
     */
    GameObject();

    /**
     * @brief   Construct a new Game object
     *  
     * @param   _x              X coordinate
     * @param   _y              Y coordinate
     * @param   _symbol         Unique symbol representing GameObject
     * @param   _encounterText  Text shown each time this object is encoutered
     * @param   _interactText   Text shown each time this object is interacted with
     */
    GameObject(int _x, int _y, char _symbol, std::string _encounterText, std::string _interactText);


    /**
     * @brief   Gets GameObject's position
     * @return  Position    Returning current GameObject's position 
     */
    Position GetPosition();

    /**
     * @brief   Updates GameObject's position
     * @param   _position     New position to be set
     */
    void SetPosition(const Position & _position);

    /**
     * @brief   Get the Encounter Text of GameObject
     * @return  std::string     Returning correct encounter text
     */
    std::string GetEncounterText();

    /**
     * @brief   Get the Interact Text of GameObject
     * @return  std::string     Returning correct interact text
     */
    std::string GetInteractText();

    /**
     * @brief   Purely virtual draw function. Each object should know,
     *          how to print itself into a window. Each object has different
     *          ncruses color, so it will now what to print
     * @param   _window     Pointer to window to draw to
     */
    virtual void Draw(WINDOW * _window) = 0;
    
    /**
     * @brief   Drawing into the ncurses window encountered event. By default,
     *          draw nothing.
     * @param   _window     Ncurses window to draw to
     */
    virtual void DrawEncounter(WINDOW * _window);

    /**
     * @brief   This virtual function has to be implemented in each class that
     *          inherits from this one. Each GameObject reacts differently to
     *          collisions with other GameObjects. This method is called on this object
     *          if some other GameObject collided with it.
     * @param   _other  Pointer to GameObject, that collided with Goblin
     * @return  true    GameObject should be destroyed after this interaction
     * @return  false   GameObject should not be destroyed after this interaction
     */
    virtual bool Interact(const std::shared_ptr<Player> & _player) = 0;

    /**
     * @brief   Virtual method for saving the current state of GameObject.
     *          Each class that derives from this should also know how to 
     *          save it's own values into a text file
     * @param   _saveFile   Text file where to store data
     */
    virtual void Save(FileHandler & _saveFile);

    /**
     * @brief   Virtual method for laoding previously saved state of GameObject.
     *          Each class that derives from this should also know how to 
     *          load it's own values into a text file
     * @param   _saveFile   Text file from where to load data
     */
    virtual void Load(FileHandler & _saveFile);
};