# Realeased versions

## 1.0.0 - Full release

- Fixed unexpected behaviour when save or map file were corrupted or empty
- Fixed no player definition in save or map file
- Code cleanup, refactor
- Modified doxyfile, from now on doc is generated into ../doc folder

## 0.0.6

- Almost ready for release
- Code refactoring
- README.md updated
- Inheritance diagrams updated
- Inventory, item picking up and storing to inventory
- Save / Load to a text file
- FileHandler implemented - checking the integrity of a save file
- Tweaked GameSystem's map boundary checks
- Experience gains after succesful fight
- New dungeon map (map1.txt)
- Unused function and classes removed (Legend.hpp, etc.)
- Lot more stuff and cleaning up...

## 0.0.5

- Save the game into a text file
- In game menu for saving and quitting the game
- Goblins are now chasing player
- Player stopped inheriting from Character
- Redesigned the scene, now also keeping track of NPCs in a map
- Player shared_ptr stored separately
- Fighting system done - each monster attacks differently, defenses are taken into account
- Switched around methods from GameObject to Character, to follow the Liskov substituion principle

## 0.0.4

- When player collides with GameObjects, InfoPanel now gets update with information
- Random names fantasy names for Dragons, Ghosts and Goblins
- Encounter and interact texts
- Monsters are moving now, randomly per now
- Fixed updates in scene with old and new position. Using C++17 extract for key change

## 0.0.3

- Map file corruption detection - need more testing
- Player movement, basic collision checking
- Default values of health, attack damage, ability power etc are now part of each Class instead of global declaration in Contanst.hpp

## 0.0.2

- Modified a bit README.md file better explaining the goal of the game
- Fixed drawing errors to the window (replaced ncurses mvwprint with mvwaddch)
- Unique colors for each GameObject
- Working drawing with ncurses colors
- HUD updated - Added informational panel and Legend with colors

## 0.0.1

- Processing of source map text files. Dynamically allocating different classes and storing them in polymorhic map.
- Printing of the scene, currently unable to moves. Colors doesn't work
- NonMovableObjectException which now throws all GameObjects, that are non movable e.g. Chests, Walls, Items.
- InternalErrorException exception added to solve cases that should not occur
- Different "Monster" classes e.g. Goblin, Dragon and Ghost. Each have different starting values in Constants.hpp and in future will have different variables
- Different "Unmovable object" classes e.g. Item, Chest, Trap, Wall. Will be developed in future releases
- Scene vector removed. As of now, represanting the scene only in map with key as Coordinates and value are GameObjects
- Monster class removed. Each monster is now inheriting directly from Character class. Monster class did not add any variables or methods and was useless

## 0.0.0

- As of now the program is runnable, use `make all`
- Basic program structure and game architecture design implemented
- Ncurses GUI almost done - Welcome screen, Main menu, Player creation screen
- Custom exceptions and their handling - only ncurses are memory leaking
- All important variables can be controlled via Constants.hpp file
- Inheritance logic implemented - Scene represented as a polymorphic vector of GameObjects
- Use of ASCII art in ncurses menus
- Game loop logic implemented, scrolling in Main Menu, responsive Main Menu
- Documentation, README, CHANGELOG