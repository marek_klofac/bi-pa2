# Graphical RPG

This is landing page for this semestral project of BI-PA2 containing useful links. Also you can find full task description here.

## Up-to-date changes

Please see all implemented and removed features here -> [CHANGELOG](./CHANGELOG.md)

## Prerequisites

Important prerequsites for the program to run. Please donwload them, else you won't be able to run the program. Doxygen and GraphViz are optional, although it's impossible to generate documentation without them.

- Ncurses - used for graphical user interface, program won't run without it
    ```ruby
    sudo apt-get install libncurses5-dev libncursesw5-dev
    ```
- Doxygen - used for documentation generation from code
    ```ruby
    sudo apt-get install doxygen
    ```
- GraphViz - also used for documentation generation (UML and inheritance diagrams)
    ```ruby
    sudo apt-get install graphviz
    ```

## Install

- Clone this repo
- Use `make all` to compile, generate documentation and run the program


## Concept

This project outcome will be a turn based graphical RPG. The main features of this game can be found below. I have decided to use ncurses as a GUI because of library limitations on ProgTest.

It is a text based RPG. Player can move freely around and his ultimate goal is to kill all Monsters. Some of them are trying to hunt him, others are just wandering about. Player can pick up Items that will boost his abilities and help him in fights. Upon killing a monster, Player gains experience points increasing his Level -> meaning his base health, attack damage and defense will increase.

HUD helps User to keep track of the current situation. On the right hand side he can find Legend -> overview of all symbols that can appear on the screen. At the bottom there is a informational display providing user with current situation, informing him about actual events. 

As I said, this game is turn-based. Upon receiveing a input from user's keyboard, next state of the game is calculated. Player can encounter different Monsters, traps and items. He is able to interact with them - fightning with Monsters, stepping into traps and picking up items and storing them in Inventory.

Player can move around freely using arrow keys on his keyboard. The game ends, once all Monsters are killed, meaning the level is cleared. The game can end preemptively when Player's health is below 0. That means the player failed to complete the game and can try again.

During gameplay, Player can bring up Menu pressing 'ESC' key to save his current progress or quit. The state of the game is saved into a text file which can be loaded again from Main Menu if user chooses so. Please note, that any modification of this save file will lead to it's corruption and it will be lost.

Initially, the map is read from a text file containing special characters matching different GameObjects. Below you can see a Legend. This opens possibilities for custom map making. The only limitations are width and height of the level (because of ncurses) and characters that can be used. For custom maps please use this default template -> [Default map](./src/maps/default_map.txt)

| Symbol | GameObject |
|:------:|:-----------|
| @      | Player     |
| #      | Wall       |
| $      | Item       |
| &      | Trap       |
| G      | Goblin     |
| D      | Dragon     |
| H      | Ghost      |

## Main features

- Loading of a Scene (map) defined in a text file
- Save/Load of a current state of the game
- Map and Savefile checks for integrity
- Basic and Advanced AI - Monsters wandering around vs. Monsters chasing player
- Custom Exceptions for the whole game system
- Custom map making possibilities
- Colorful nCurses GUI
- Player can customize different abilities of his character
- Fight system - Player can earn experience by killing monsters
- Simple inventory - Player can pick up items which will help him in fights
- Info panel that keeps user on track with the game state

## Polymorphism

Here I would like to discuss used polymorphism in this project. I have 2 main use of polymorphism.

### Scene represantation

I have decided to represent the whole game world's scene as a map using Coordinates as keys and values are GameObjects. This allows me to solve collisions in logarithmic time. Also in some cases, I am able to behave to them singularly and call for example Draw on all objects in the scene. See below list of virtual methods, Please see iheritance diagram below for better understanding.

- Draw for printing object's symbol to appropriate place on the screen using it's predefined unique color
- Interact for interaction with the player. Each GameObject reacts differently
- Save local variables into a text file. Derived classes implements their own Save which overrides this, but from it GameObject::Save is called. Each class should now how to save it's variables
- Load stored variables form a text file. Derived classes implements their own Load which overrides this, but from it GameObject::Load is called. Each class should now how to load it's variables
- NextPosition for calculation of next position where the object would move to (then solving any collisions)

The workflow of game loop with this setup is pretty easy (thanks to inheritance and polymorphism). 

- Clear the window
- Draw actual state of the scene
- Wait for user input
- Move with every other GameObject in the scene. Solve collisions using overriden Interact method
- Repeat until the game is over (player is dead or all monsters are dead)

Please see inheritance diagram below for better explanation of the situation. Using this represantation allows us to treat different GameObjects the same and simply call overriden methods on them.

![BaseException Inheritance](./images/gameobject_inheritance.JPG)

### Custom Exceptions

I have implemented custom exception system. I have a BaseException class, which overloads operator << for output and stores ID of exception and raised message. From this class, other specific exceptions classes inherit e.g. SaveFileException, MapFileException, NoColorTermException etc. 

This allows me to catch references to BaseException class and still output the correct raised message and message ID. Below you can find inheritance diagram.

![BaseException Inheritance](./images/baseexception_inheritance.JPG)

## Documentation

Through out the project I have been using doxygen style comments for later doxygen documentation generation. You can generated it using `make doc`